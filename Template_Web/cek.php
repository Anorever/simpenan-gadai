<!DOCTYPE html>
<html lang="en">
<head>

     <title>SIMPENAN GADAI</title>

     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

     <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/font-awesome.min.css">
     <link rel="stylesheet" href="css/animate.css">
     <link rel="stylesheet" href="css/owl.carousel.css">
     <link rel="stylesheet" href="css/owl.theme.default.min.css">
     <link rel="stylesheet" href="css/magnific-popup.css">

     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
     <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

     <!-- MAIN CSS -->
     <link rel="stylesheet" href="css/templatemo-style2.css">
    
</head>
<body>

     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

               </div>

               

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                         <img src="images/Header_Pegadaian.png" height="8%" width="18%">
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                         <li><a href="index.php#home" class="smoothScroll">Home</a></li>
                         <li><a href="index.php#contact" class="smoothScroll">Lokasi</a></li>
                         <li><a href="index.php#team" class="smoothScroll">Syarat dan Ketentuan</a></li>
                         <li><a href="index.php#bantuan" class="smoothScroll">Bantuan</a></li>    
                         <li><a href="index.php#tentangkami" class="smoothScroll">Tentang Kami</a></li>            
                    </ul>
               </div>

          </div>
     </section>

  
    <?php
    // $var = 1;
    $var = $_POST['layout_select'];
    $var2 = $_POST['versi_select'];
    $pembayaran = $_POST['pembayaran'];

    //Rest of the vars

    $col = $_POST['column_select']; 
    $layout = $_POST['layout_select'];
    $type = $_POST['type_select'];
    $warna = floatval($_POST['warna_select']);
    if($warna=="25"){
         $warna_desc="Putih";
    }
    else if($warna=="50"){
          $warna_desc="Gold";
    }
    else if($warna=="75"){
          $warna_desc="Pink";
    }

    $ver = $_POST['versi_select'];
    if($ver=="0"){
          $ver_desc="< Kitkat";
     }
    else if($ver=="25"){
          $ver_desc="Kitkat";
     }
    else if($ver=="50"){
          $ver_desc="Lollipop";
     }
    else if($ver=="75"){
          $ver_desc="Marshmallow";
     }
    else if($ver=="100"){
          $ver_desc="> Nougat";
     }

    $jns_bayar = $_POST['pembayaran'];
     if($jns_bayar=="1"){
          $jns_bayar_desc="Lunas";
     }
     else if($jns_bayar=="0"){
          $jns_bayar_desc="Cicilan";
     }

    // echo($col.$layout.$type.$warna.$ver.$jns_bayar."\n");

    $tP1 = $_POST['tampilan_depan1'];
     if($tP1=="100"){
          $tP1_desc = "Lampu Indikator Hidup";
     }
     else if($tP1=="0"){
          $tP1_desc = "Lampu Indikator Mati";
     }
     else if($tP1=="75"){
          $tP1_desc = "Tidak Terdapat Lampu Indikator";
     }
    $tP2 = $_POST['tampilan_depan2'];
     if($tP2=="100"){
          $tP2_desc = "Port USB Berfungsi";
     }
     else if($tP2=="0"){
          $tP2_desc = "Port USB Tidak Berfungsi";
     }

    $tP3 = $_POST['tampilan_depan3'];
     if($tP3=="100"){
          $tP3_desc = "Layar Baik";
     }
     else if($tP3=="0"){
          $tP3_desc = "Layar Retak";
     }
     else if($tP3=="25"){
          $tP3_desc = "Layar Terdapat Bintik Hitam";
     }

    // echo($tP1.$tP2.$tP3."\n");

    $tB1 = $_POST['tampilan_belakang1'];
    if($tB1=="100"){
     $tB1_desc = "Kamera Berfungsi";
     }
     else if($tB1=="0"){
          $tB1_desc = "Kamera Tidak Berfungsi";
     }
    $tB2 = $_POST['tampilan_belakang2'];
     if($tB2=="100"){     
          $tB2_desc = "Tombol Berfungsi";
     }
     else if($tB2=="0"){
          $tB2_desc = "Tombol Tidak Berfungsi";
     }

    $tB3 = $_POST['tampilan_belakang3'];
     if($tB3=="100"){
          $tB3_desc = "Lampu Flash Hidup";
     }
     else if($tB3=="0"){
          $tB3_desc = "Lampu Flash Mati";
     }

    $tB4 = $_POST['tampilan_belakang4'];
     if($tB4=="100"){
          $tB4_desc = "Casing Baik";
     }
     else if($tB4=="50"){
          $tB4_desc = "Casing Lecet";
     }
     else if($tB4=="0"){
          $tB4_desc = "Casing Retak";
     }


    // echo($tB1.$tB2.$tB3.$tB4."\n");

    $K1 = $_POST['kelengkapan1'];
     if($K1=="100"){
          $K1_desc = "Kotak Ada";
     }
     else if($K1=="75"){
          $K1_desc = "Kotak Tidak Ada";
     }

    $K2 = $_POST['kelengkapan2'];
     if($K2=="100"){
        $K2_desc = "Baterai Baik";
     }
     else if($K2=="75"){
          $K2_desc = "Baterai Rusak";
     }
    $K3 = $_POST['kelengkapan3'];
    if($K3=="100"){
          $K3_desc = "Chager Baik dan Normal";
     }
     else if($K3=="0"){
          $K3_desc = "Chager Rusak/Hilang";
     }

    // echo($K1.$K2.$K3."\n");

     // $tipe = substr($_POST['tipe'],13);
     $merek = '';
     if (strpos($type, 'Samsung') !== false) {
       $merek='Samsung';
     }
     else if (strpos($type, 'Iphone') !== false) {
       $merek='Iphone';
     }
     else if (strpos($type, 'Xiaomi') !== false) {
       $merek='Xiaomi';
     }
     else if (strpos($type, 'Oppo') !== false) {
       $merek='Oppo';
     }
     else if (strpos($type, 'Vivo') !== false) {
       $merek='Vivo';
     }

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname2 = "sim-gade";
    // Create connection
    $conn2 = new mysqli($servername, $username, $password, $dbname2);
    // Check connection
    if ($conn2->connect_error) {
        echo (die("Connection failed: " . $conn2->connect_error));
    }
    $layak="Layak";
    $sql = 'INSERT INTO fuzzy_data (produk,merek,tipe,warna,versi_android,jenis_bayar,tampilan_depan1,tampilan_depan2,tampilan_depan3,tampilan_belakang1,tampilan_belakang2,tampilan_belakang3,tampilan_belakang4,kelengkapan1,kelengkapan2,kelengkapan3,Kelayakan) VALUES
    ("'.$col.'","'.$layout.'","'.$type.'","'.$warna_desc.'","'.$ver_desc.'","'.$jns_bayar_desc.'","'.$tP1_desc.'","'.$tP2_desc.'","'.$tP3_desc.'","'.$tB1_desc.'","'.$tB2_desc.'","'.$tB3_desc.'","'.$tB4_desc.'","'.$K1_desc.'","'.$K2_desc.'","'.$K3_desc.'","'.$layak.'")';
    mysqli_query($conn2, $sql);

    
     // if ($conn2->query($sql) === TRUE) {
     //      echo "New record created successfully";
     // } else {
     //      echo "Error: " . $sql . "<br>" . $conn2->error;
     // }
    
//     $layak="Layak";
//     $sql = 'INSERT INTO fuzzy_data (produk,merk,tipe,versi_android,jenis_bayar,tampilan_depan1,tampilan_depan2,tampilan_depan3,tampilan_belakang1,tampilan_belakang2,tampilan_belakang3,tampilan_belakang4,kelengkapan1,kelengkapan2,kelengkapan3,Kelayakan) VALUES
//     ("'.$col.'","'.$layout.'","'.$type.'","'.$warna_desc.'","'.$ver_desc.'","'.$jns_bayar_desc.'","'.$tP1_desc.'","'.$tP2_desc.'","'.$tP3_desc.'","'.$tB1_desc.'","'.$tB2_desc.'","'.$tB3_desc.'","'.$tB4_desc.'","'.$K1_desc.'","'.$K2_desc.'","'.$K3_desc.'","'.$layak.'")';
//     mysqli_query($conn2, $sql);

    // echo shell_exec("python fuzzy.py $var 2");
    // echo($warna." + ".$ver." + ".$tP1." + ".$tP2." + ".$tP3." + ".$tB1." + ".$tB2." + ".$tB3." + ".$tB4." + ".$K2." + ".$K3);
    
    $tipe = substr($_POST['type_select'],13);
//     echo($tipe);
    $fuzzy = exec("python fuzzy.py $warna $ver $tP1 $tP2 $tP3 $tB1 $tB2 $tB3 $tB4 $K2 $K3");
    $regresi = exec("python regression.py $tipe");


//     echo('<h1 class="text-center" style="padding-top:20%;">'.$regresi.'</h1>');
    
    ?><div class="container text-center ">
        <h3 id="greentext" data-aos="fade-up" style="color:green; font-size:20px; padding-bottom:0%; padding-top:10%;">Barang anda dinyatakan</h3>
        <br><h1 id="greentext" data-aos="fade-up" style="color:green; font-size:25px; padding-top:0%;">
    <?php

    if($fuzzy<40){
        echo ('"TIDAK LAYAK GADAI!"
        </h1></div>

        ');
        $layak="Tidak Layak";
        $sql = 'INSERT INTO fuzzy_data (produk,merek,tipe,warna,versi_android,jenis_bayar,tampilan_depan1,tampilan_depan2,tampilan_depan3,tampilan_belakang1,tampilan_belakang2,tampilan_belakang3,tampilan_belakang4,kelengkapan1,kelengkapan2,kelengkapan3,Kelayakan) VALUES
        ('.$col.'","'.$layout.'","'.$type.'","'.$warna_desc.'","'.$ver_desc.'","'.$jns_bayar_desc.'","'.$tP1_desc.'","'.$tP2_desc.'","'.$tP3_desc.'","'.$tB1_desc.'","'.$tB2_desc.'","'.$tB3_desc.'","'.$tB4_desc.'","'.$K1_desc.'","'.$K2_desc.'","'.$K3_desc.'","'.$layak.'")';
        mysqli_query($conn2, $sql);
    }
    else{
     $layak="Layak";
     $sql = 'INSERT INTO fuzzy_data (produk,merek,tipe,warna,versi_android,jenis_bayar,tampilan_depan1,tampilan_depan2,tampilan_depan3,tampilan_belakang1,tampilan_belakang2,tampilan_belakang3,tampilan_belakang4,kelengkapan1,kelengkapan2,kelengkapan3,Kelayakan) VALUES
     ('.$col.'","'.$layout.'","'.$type.'","'.$warna_desc.'","'.$ver_desc.'","'.$jns_bayar_desc.'","'.$tP1_desc.'","'.$tP2_desc.'","'.$tP3_desc.'","'.$tB1_desc.'","'.$tB2_desc.'","'.$tB3_desc.'","'.$tB4_desc.'","'.$K1_desc.'","'.$K2_desc.'","'.$K3_desc.'","'.$layak.'")';
     //     echo($col."+".$layout."+".$type."+".$warna_desc."+".$ver_desc."+".$jns_bayar_desc."+".$tP1_desc."+".$tP2_desc."+".$tP3_desc."+".$tB1_desc."+".$tB2_desc."+".$tB3_desc."+".$tB4_desc."+".$K1_desc."+".$K2_desc."+".$K3_desc."+".$layak);

     mysqli_query($conn2, $sql);

        $output = [];
        $timestamp = time();
        for ($i = 0 ; $i < 7 ; $i++) {
                $output[] = date('d-m-Y', $timestamp);
                $timestamp += 24 * 3600;
        }
        
        $tipe = substr($_POST['type_select'],13);
        $merek = $_POST['layout_select'];

        if($_POST['layout_select']=="handphone-1"){
               $merek="Samsung";
        }
        else if($_POST['layout_select']=="handphone-2"){
               $merek="Iphone";
        }
        else if($_POST['layout_select']=="handphone-3"){
               $merek="Xiaomi";
        }
        else if($_POST['layout_select']=="handphone-4"){
               $merek="Oppo";
        }
        else if($_POST['layout_select']=="handphone-5"){
               $merek="Vivo";
        }

        $versi = $_POST['versi_select'];
        if($_POST['versi_select']=="75"){
             $versi="Marshmallow";
        }

        echo ('"LAYAK GADAI!"
    
    </h1>
    </div>


    <div class="container" style="padding-left:20%; padding-right:20%;">
    <form  method="POST" action="pdfgenerator.php" name="frm"> 
        <div class="form-group">
            <h3 style="padding:54x;">Detail Barang Gadai :</h3>
            <label for="inputAddress">Merek</label>
            <input type="text" class="form-control" value="'. $merek .'" name="merek" placeholder="Nama" readonly>
        </div>
        <div class="form-group">
            <label for="inputAddress">Tipe</label>
            <input type="text" class="form-control" value="'. $tipe .'" name="tipe" placeholder="Nama" readonly>
        </div>
        <div class="form-group">
          <label for="inputAddress">Versi Android</label>
          <input type="text" class="form-control" value="'. $versi .'" name="versi" placeholder="Nama" readonly>
        </div>
        <div class="form-group">
            <label for="inputAddress">Nama</label>
            <input type="text" class="form-control" name="nama" placeholder="Nama" required>
        </div>
        <div class="form-group">
            <label for="inputAddress2">Alamat Email</label>
            <input type="text" class="form-control" name="email" placeholder="Email" required>
        </div>
        <div class="form-group">
            <label for="inputAddress">Nomor Telepon</label>
            <input type="text" class="form-control" name="telp" placeholder="telp" required>
            <input type="text" value="'. $regresi .'" name="range" hidden>
        </div>
        <div class="form-group">
            <h3 style="padding:54x;">Pesan No. Antrian Gadai :</h3>
            <label for="inputAddress">Tanggal</label><br>
            <select name="days" class="form-control" placeholder="Email">');
        
        foreach($output as $day):        
            echo ('<option value="'.$day.' ">'. $day.'</option>');
        endforeach; 
        
        echo('
            </select>
        </div>
        <div class="form-group">
            <label for="inputAddress2">Jam</label>
            <select name="times" class="form-control" placeholder="jam">');

        for($hours=8; $hours<15; $hours++) // the interval for hours is '1'
         for($mins=0; $mins<60; $mins+=30) // the interval for mins is '30'
             echo '<option>'.str_pad($hours,2,'0',STR_PAD_LEFT).':'
                            .str_pad($mins,2,'0',STR_PAD_LEFT).'</option>';
        echo('</select>
        </div></div>
        <div class="container" style="padding-left:20%; padding-right:20%; ">

        <div class="form-group">
            <h3 style="padding:54x;">Detail Outlet:</h3>
            <label for="inputAddress">Nama Outlet</label><br>
               <select class="form-control select2 btn-primary btn-lg" style="width:100%; height:100px;" name="Nama_outlet" onChange="myNewFunction(this);">
                    <option>Nama Outlet</option> 
                    ');
          include "upc.php";
          echo('</select>
            ');
     
     ?>
          <script>
                    function myNewFunction(sel) {
                    if(sel.options[sel.selectedIndex].text=="UPC Kemanggisan"){
                              var alamat_outlet = document.getElementById("alamatOutlet").value = "Jalan Kemanggisan";
                              var alamat_outlet = document.getElementById("kodeCabang").value = "12300";}
                    else{

                              var alamat_outlet = document.getElementById("alamatOutlet").value =  "Jalan" + sel.options[sel.selectedIndex].text.substring(3);
                              var alamat_outlet = document.getElementById("kodeCabang").value = "00002";
                    }
                   
                    }

          </script>
          
          
     <?php
     echo('
        <input type="hidden" class="form-control" id="alamatOutlet" value="Alamat Outlet" name="alamat_outlet" placeholder="alamat_outlet" readonly>
        <input type="hidden" class="form-control" id="kodeCabang" value="Alamat Outlet" name="kode_cabang" placeholder="alamat_outlet" readonly>
        </div>
        <a href="index.php#contact" target="_blank"><u>Lihat Outlet Pegadaian Terdekat</u></a> <br><br>
        <button type="submit" class="btn btn-primary" style="text-align:right;">Kirim Estimasi</button>
        </div>
    </form>
    </div>
    ');
    
    }
    ?>
    
      <!-- FOOTER -->
      <footer id="footer" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-8">
                         <div class="footer-info">
                              <div class="section-title">
                              <h5 class="font-weight-bold text-uppercase mb-2 wow fadeInUp" data-wow-delay="0.2s" >SIMPENAN GADAI</h5>
                              <p  class="wow fadeInUp" data-wow-delay="0.2s"><em>"Sistem Pengambil Keputusan Gadai"</em></p>
                              <p  class="wow fadeInUp" data-wow-delay="0.2s">Tugas Mini Project - SPRINT Angakatan 1 2020</p>
                              </div>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-8">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2 class="wow fadeInUp" data-wow-delay="0.2s">Alamat</h2>
                              </div>
                              <ul class="list-unstyled wow fadeInUp" data-wow-delay="0.2s">
                                   <li>
                                   <p>
                                        <i class="fas fa-home mr-3"></i> Kantor Pusat: Jl. Kramat Raya 162 Jakarta Pusat 10430 </p>
                                   </li>
                                   <li>
                                   <p>
                                        <i class="fas fa-envelope mr-3"></i> pegadaian@pegadaian.co.id</p>
                                   </li>
                                   <li>
                                   <p>
                                        <i class="fas fa-phone mr-3"></i> 0213155550 | 02180635162 </p>
                                   </li>
                              </ul>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                         <br>
                         <h5 class="font-weight-bold text-uppercase mb-2 wow fadeInUp" style="padding-left:35%;" data-wow-delay="0.8s">Follow Us</h5><br>
                              <a type="button" class="btn-floating btn-fb fa-2x wow fadeInUp" style="padding-left:35%;" data-wow-delay="0.8s">
                              <i class="fab fa-facebook-f ico"></i>
                              </a> 
                              <a type="button" class="btn-floating btn-tw fa-2x wow fadeInUp" data-wow-delay="0.8s">
                                   <i class="fab fa-twitter ico"></i>
                              </a>
                              <a type="button" class="btn-floating btn-instagram fa-2x wow fadeInUp" data-wow-delay="0.8s">
                                   <i class="fab fa-instagram ico"></i>
                              </a>
                         <div class="wow fadeInUp copyright-text" data-wow-delay="0.8s"> 
                              <p style="padding-left:20%;"><br>Copyright 2020 &copy; Team4SPRINT
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>
</body>
</html>
    
     <script>
        $('.select2').select2();
     </script>
      
     <script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/jquery.stellar.min.js"></script>
     <script src="js/wow.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/jquery.magnific-popup.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>

     
