<!doctype html> 
<html> 
<head> 
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
  <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

     <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/font-awesome.min.css">
     <link rel="stylesheet" href="css/animate.css">
     <link rel="stylesheet" href="css/owl.carousel.css">
     <link rel="stylesheet" href="css/owl.theme.default.min.css">
     <link rel="stylesheet" href="css/magnific-popup.css">

     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
     <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

     <!-- MAIN CSS -->
     <link rel="stylesheet" href="css/form-kelayakan.css">

  <title>Simpenan Gadai</title> 
	<style>
{box-sizing:border-box}

/* Slideshow container */
.slideshow-container {
  max-width: 700px;
  position: relative;
  /* padding-left:200px; */
  margin: auto;
}


/* Hide the images by default */
.mySlides {
  display: none;
}
.con-form {
  width:500px;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  margin-top: -22px;
  padding: 16px;
  color: black;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px 0 220px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
  
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s ;
  animation-name: fade;
  animation-duration:  1.5s;
  animation-iteration-count: infinite;
}

@-webkit-keyframes fade {
  from {opacity: 1}
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: 1}
  to {opacity: 1}
}
}

.input-icons i { 
            position: absolute; 
        } 
          
        .input-icons { 
            width: 100%; 
            margin-bottom: 10px; 
        } 
          
        .icon { 
            padding: 10px; 
            min-width: 40px; 
        } 
          
        .input-field { 
            width: 100%; 
            padding: 10px; 
            text-align: center; 
        } 
td{
    padding-bottom:5%;
    width:2px;
}

	</style>
</head>
<section id="formKelayakan" class="slider" data-stellar-background-ratio="2"> 
<body> 

     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

               </div>

               

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                         <img src="images/Header_Pegadaian.png" height="8%" width="18%">
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                         <li><a href="index.php#home" class="smoothScroll">Home</a></li>
                         <li><a href="index.php#contact" class="smoothScroll">Lokasi</a></li>
                         <li><a href="index.php#one" class="smoothScroll">Syarat dan Ketentuan</a></li>
                         <li><a href="index.php#bantuan" class="smoothScroll">Bantuan</a></li>   
                         <li><a href="index.php#tentangkami" class="smoothScroll">Tentang Kami</a></li>                               
                         <!--<a href="#footer" class="section-btn">Login</a>-->
                    </ul>
               </div>

          </div>
     </section>

<div class="container" >
  <div class="row">
    <div class="col col-lg-3">

    <div class="container con-form"  >
      <h3 style="padding-bottom:2%;">Formulir Estimasi <br>Barang Gadai</h3>
      <!-- <form action="/action_page.php"> -->
      <form  method="POST" action="cek.php" name="frm"> 
      <table id="tbl" >
                                    <tr>
                                        <td><strong>Produk </strong></td>
                                        <td>: </td>
                                        <td>

                                            <select name="column_select" id="column_select">
                                                <option value="handphone">handphone</option>
                                                <option value="mobil">mobil</option>
                                                <option value="motor">motor</option>
                                                <option value="laptop">laptop</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Merk</strong></td>
                                        <td>: </td>
                                        <td>

                                        <select name="layout_select" class="layout_select coloumn_select" id="layout_select">
                                        <option value="empty">Pilih Merk</option>
                                            <!--Below shows when '1 column' is selected is hidden otherwise-->
                                            <!-- <option>Select Product</option> -->
                                            <option value="mobil-1">Honda</option>
                                            <option value="mobil-2">Toyota</option>
                                            <option value="mobil-3">Daihatsu</option>
                                            <option value="mobil-4">Suzuki</option>
                                            <option value="mobil-5">Mitsubishi</option>

                                            <!--Below shows when '2 column' is selected is hidden otherwise-->
                                            <option value="motor-1">Honda</option>
                                            <option value="motor-2">Yamaha</option>
                                            <option value="motor-3">Suzuki</option>
                                            <option value="motor-4">Vespa</option>
                                            
                                            <!--Below shows when '3 column' is selected is hidden otherwise-->
                                            <option value="laptop-1">HP</option>
                                            <option value="laptop-2">Apple</option>
                                            <option value="laptop-3">Dell</option>
                                            <option value="laptop-4">Lenovo</option>
                                            <option value="laptop-5">Asus</option>
                                        
                                            <option value="handphone-1">Samsung</option>
                                            <option value="handphone-2">Iphone</option>
                                            <option value="handphone-3">Xiaomi</option>
                                            <option value="handphone-4">Oppo</option>
                                            <option value="handphone-5">Vivo</option>        
                                        </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Tipe :</strong></td>
                                        <td>: </td>
                                        <td>
                                        <select name="type_select" id="type_select" >
                                        <option value="empty">Pilih Tipe</option>                                           
                                                <option value="mobil-1-1">Honda BRV</option>
                                                <option value="mobil-1-2">Honda Jazz</option>
                                                <option value="mobil-1-3">Honda Brio</option>
                                                <option value="mobil-1-4">Honda CRV</option>
                                                <option value="mobil-1-5">Honda HRV</option>

                                                <option value="mobil-2-1">Toyota Agya</option>
                                                <option value="mobil-2-2">Toyota Calya</option>
                                                <option value="mobil-2-3">Toyota Rush</option>
                                                <option value="mobil-2-4">Toyota Kijang Innova</option>
                                                <option value="mobil-2-5">Toyota Avanza</option>

                                                <option value="mobil-3-1">Suzuki Karimun Wagon R</option>
                                                <option value="mobil-3-2">Suzuki APV</option>
                                                <option value="mobil-3-3">Suzuki Ignis</option>
                                                <option value="mobil-3-4">Suzuki Carry</option>
                                                <option value="mobil-3-5">Suzuki Ertiga</option>

                                                <option value="mobil-4-1">Daihatsu Gran Max</option>
                                                <option value="mobil-4-2">Daihatsu Luxio</option>
                                                <option value="mobil-4-3">Daihatsu Sigra</option>
                                                <option value="mobil-4-4">Daihatsu Sirion</option>
                                                <option value="mobil-4-5">Daihatsu Ayla</option>

                                                <option value="mobil-5-1">Mitsubishi Pajero Sport</option>
                                                <option value="mobil-5-2">Mitsubishi Eclipse Cross</option>
                                                <option value="mobil-5-3">Mitsubishi Xpander</option>
                                                <option value="mobil-5-4">Mitsubishi Triton 4X4</option>
                                                <option value="mobil-5-5">Mitsubishi Outlander Sport</option>
        

                                                <option value="motor-1-1">Honda Vario</option>
                                                <option value="motor-1-2">Honda Beat</option>
                                                <option value="motor-1-3">Honda Revo</option>
                                                <option value="motor-1-4">Honda Supra X</option>
                                                <option value="motor-1-5">Honda Scoopy</option>

                                                <option value="motor-2-1">Yamaha Mio</option>
                                                <option value="motor-2-2">Yamaha Nmax</option>
                                                <option value="motor-2-3">Yamaha Aerox</option>
                                                <option value="motor-2-4">Yamaha Byson</option>
                                                <option value="motor-2-5">Yamaha R15</option>

                                                <option value="motor-3-1">Suzuki Raider</option>
                                                <option value="motor-3-2">Suzuki Smash</option>
                                                <option value="motor-3-3">Suzuki Gsx</option>
                                                <option value="motor-3-4">Suzuki Satria</option>
                                                <option value="motor-3-5">Suzuki Address</option>

                                                <option value="motor-4-1">Vespa GTS</option>
                                                <option value="motor-4-2">Vespa Sprint</option>
                                                <option value="motor-4-3">Vespa Primavera</option>
                                                <option value="motor-4-4">Vespa S</option>
                                                <option value="motor-4-5">Vespa LX</option>

                                                <option value="motor-5-1">Kawasaki Ninja RR</option>
                                                <option value="motor-5-2">Kawasaki Ninja W175</option>
                                                <option value="motor-5-3">Kawasaki Z125</option>
                                                <option value="motor-5-4">Kawasaki Ninja R150</option>
                                                <option value="motor-5-5">Kawasaki Ninja Z250</option>

                                                <option value="laptop-1-1">HP Pavilion X360</option>
                                                <option value="laptop-1-2">HP Envy 13</option>
                                                <option value="laptop-1-3">HP Mini 110</option>
                                                <option value="laptop-1-4">HP Spectre Folio</option>
                                                <option value="laptop-1-5">HP 430</option>

                                                <option value="laptop-2-1">Apple MacBook 12</option>
                                                <option value="laptop-2-2">Apple MacBook ProMC700ZA/A</option>
                                                <option value="laptop-2-3">Apple MacBook Pro 13.3 Touch Bar</option>
                                                <option value="laptop-2-4">Apple MacBook Pro</option>
                                                <option value="laptop-2-5">Apple MacBook Air</option>

                                                <option value="laptop-3-1">Dell Inspiron 3162</option>
                                                <option value="laptop-3-2">Dell Vostro 14-3468</option>
                                                <option value="laptop-3-3">Dell Gaming G3 3579</option>
                                                <option value="laptop-3-4">Dell Inspiron 7562</option>
                                                <option value="laptop-3-5">Dell Inspiron 11-3179</option>

                                                <option value="laptop-4-1">Lenovo Legion</option>
                                                <option value="laptop-4-2">Lenovo Yoga Series</option>
                                                <option value="laptop-4-3">Lenovo Ideapad 300s</option>
                                                <option value="laptop-4-4">Lenovo V330</option>
                                                <option value="laptop-4-5">Lenovo ThinkPad X</option>

                                                <option value="laptop-5-1">ASUS X550Z</option>
                                                <option value="laptop-5-2">ASUS VivoBook Ultra A412</option>
                                                <option value="laptop-5-3">ASUS X200CA</option>
                                                <option value="laptop-5-4">ASUS ROG GL552JX</option>
                                                <option value="laptop-5-5">ASUS X453SA</option>

                                                <option value="handphone-1-1Samsung A30S">Samsung A30S</option>
                                                <option value="handphone-1-2Samsung A50S">Samsung A50S</option>
                                                <option value="handphone-1-3Samsung A9">Samsung A9</option>
                                                <option value="handphone-1-4Samsung A50">Samsung A50</option>
                                                <option value="handphone-1-5Samsung A80">Samsung A80</option>

                                                <option value="handphone-2-1">Iphone X 64GB</option>
                                                <option value="handphone-2-2">Iphone 8+ 64GB</option>
                                                <option value="handphone-2-3">Iphone 8 64GB</option>
                                                <option value="handphone-2-4">Iphone 7+</option>
                                                <option value="handphone-2-5">Iphone 7 64GB</option>

                                                <option value="handphone-3-1">Xiaomi Redmi 7A</option>
                                                <option value="handphone-3-2">Xiaomi 5A</option>
                                                <option value="handphone-3-3">Xiaomi Note 8</option>
                                                <option value="handphone-3-4">Xiaomi Mi A1</option>
                                                <option value="handphone-3-5">Xiaomi Mi 8</option>

                                                <option value="handphone-4-1">Oppo F7</option>
                                                <option value="handphone-4-2">Oppo F9</option>
                                                <option value="handphone-4-3">Oppo F11</option>
                                                <option value="handphone-4-4">Oppo A5S</option>
                                                <option value="handphone-4-5">Oppo A3S</option>

                                                <option value="handphone-5-1">Vivo Y95</option>
                                                <option value="handphone-5-2">Vivo Y15</option>
                                                <option value="handphone-5-3">Vivo S1</option>
                                                <option value="handphone-5-4">Vivo Z1 pro</option>
                                                <option value="handphone-5-5">Vivo V17 pro</option>
                                              
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Warna :</strong></td>
                                        <td>: </td>
                                        <td>
                                            <select name="warna_select" id="type_select">                                           
                                                  <option value="25">Putih</option>
                                                  <option value="50">Gold</option>
                                                  <option value="75">Pink</option>
                                            </select>
                                          </td>
                                    </tr>
                                    <tr>
                                          <td><strong>Versi </strong></td>
                                        <td>: </td>
                                        <td>
                                          <select name="versi_select">
                                                  <option value="0">&lt; Kitkat</option>
                                                  <option value="25">Kitkat</option>
                                                  <option value="50">Lollipop</option>
                                                  <option value="75">Marshmallow</option>
                                                  <option value="100">&gt; Nougat</option>
                                                </select>
                                          </td>
                                    </tr>
                                    <tr>
                                      <td><strong>Pembayaran</strong></td>
                                        <td>: </td>
                                        <td>
                                            <select name="pembayaran" id="pembayaran" onchange="getItems(this.value)">
                                                  <option value="1">Lunas</option>
                                                  <option value="0">Cicilan</option>
                                                </select>
                                      </td>
                                    </tr>
                                </table>
        <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
      <!-- </form> -->
    </div>

    </div>
    <div class="col">

<div class="container" style="padding-left:20%;">	
		<center>
			<h3>Form Kelayakan Barang Gadai</h3>
		</center>
    
		<div class="slideshow-container">

  <!-- Full-width images with number and caption text -->
  <div class="mySlides fade">
    <div class="numbertext">1 / 3</div>	
	<center>			
	<img  src="images/tampildepan.JPG">
	</center>
    <div style="position:relative; bottom:350px;left:50px">
            <input type="radio" id="tampil_depan1_1" name="tampilan_depan1" value="100" id="question" required> 
                <label for="tampil_depan1_1">Lampu Indikator Hidup</label><br>
            <input type="radio" id="tampil_depan1_2" name="tampilan_depan1" value="0" id="question">
                <label for="tampil_depan1_2">Lampu Indikator Mati</label><br>
            <input type="radio" id="tampil_depan1_3" name="tampilan_depan1" value="75" id="question">
                <label for="tampil_depan1_3">Tidak Terdapat<br>Lampu Indikator</label><br>
						<!-- <input  type="checkbox" name="tampil_depan1_1" value="indikatorhidup">Lampu Indikator Hidup <br />
						<input  type="checkbox" name="tampil_depan1_2" value="indikatormati">Lampu Indikator Mati <br />
						<input  type="checkbox" name="tampil_depan1_3" value="indikatortidakAda">Tidak Terdapat Lampu Indikator <br /> -->
            </div>
      <div style="position:relative; bottom:90px;left:50px">
            <input type="radio" id="tampil_depan2_1" name="tampilan_depan2" value="100" id="question" required>
                <label for="tampil_depan2_1">Port USB Berfungsi</label><br>
            <input type="radio" id="tampil_depan2_2" name="tampilan_depan2" value="0" id="question">
                <label for="tampil_depan2_2">Port USB Tidak Berfungsi</label><br>
            <!-- <input  type="checkbox" name="tampil_depan2_1" value="usbHidup">Port USB Berfungsi <br />
						<input  type="checkbox" name="tampil_depan2_2" value="usbMati">Port USB Tidak Berfungsi <br /> -->
      </div>
    <div style="position:relative; bottom:420px;left:500px">
            <input type="radio" id="tampil_depan3_1" name="tampilan_depan3" value="100" id="question" required>
                <label for="tampil_depan3_1">Layar Baik</label><br>
            <input type="radio" id="tampil_depan3_2" name="tampilan_depan3" value="0" id="question">
                <label for="tampil_depan3_2">Layar Retak</label><br>
            <input type="radio" id="tampil_depan3_3" name="tampilan_depan3" value="25" id="question">
                <label for="tampil_depan3_3">Layar Terdapat Bintik Hitam</label>
            <!-- <input  type="checkbox" name="tampil_depan3_1" value="layarBaik">Layar Baik <br />
						<input  type="checkbox" name="tampil_depan3_2" value="layarRetak">Layar Retak <br/>
            <input  type="checkbox" name="tampil_depan3_3" value="layarBintikHitam">Layar Terdapat Bintik Hitam <br/> -->
    </div>
    <div class="text" style="color:black;"><h4>Tampil Depan</h4></div>
  </div>

  <div class="mySlides fade">
    <div class="numbertext">2 / 3</div>
    <center>			
	<img  src="images/tampilanbelakang.JPG" style="height:370px">
	</center>
  <div style="position:relative; bottom:350px;left:70px">
            <input type="radio" id="tampilan_belakang1_1" name="tampilan_belakang1" value="100" id="question" required>
                <label for="tampilan_belakang1_1">Kamera Berfungsi</label><br>
            <input type="radio" id="tampilan_belakang1_2" name="tampilan_belakang1" value="0" id="question">
                <label for="tampilan_belakang1_2">Kamera Tidak<br>berfungsi</label><br>
						<!-- <input  type="checkbox" name="tampilan_belakang1_1" value="kameraHidup">Kamera Berfungsi <br />
						<input  type="checkbox" name="tampilan_belakang1_2" value="kameraMati">Kamera Tidak berfungsi <br /> -->
            </div>
      <div style="position:relative; bottom:290px;left:70px">
            <input type="radio" id="tampilan_belakang2_1" name="tampilan_belakang2" value="100" id="question" required>
                <label for="tampilan_belakang2_1">Tombol Berfungsi</label><br>
            <input type="radio" id="tampilan_belakang2_2" name="tampilan_belakang2" value="0" id="question">
                <label for="tampilan_belakang2_2">Tombol Tidak<br>Berfungsi</label><br>
            <!-- <input  type="checkbox" name="tampilan_belakang2_1" value="tombolHidup">Tombol Berfungsi <br />
						<input  type="checkbox" name="tampilan_belakang2_2" value="tombolMati">Tombol Tidak Berfungsi <br /> -->
      </div>
    <div style="position:relative; bottom:480px;left:500px">
            <input type="radio" id="tampilan_belakang3_1" name="tampilan_belakang3" value="100" id="question" required>
                <label for="tampilan_belakang3_1">Lampu Flash Hidup</label><br>
            <input type="radio" id="tampilan_belakang3_2" name="tampilan_belakang3" value="0" id="question">
                <label for="tampilan_belakang3_2">Lampu Flash Mati</label><br>
            <!-- <input  type="checkbox" name="tampilan_belakang3_1" value="flashHidup">Lampu Flash Hidup <br />
						<input  type="checkbox" name="tampilan_belakang3_2" value="flashMati">Lampu Flash Mati <br/> -->
    </div>
    <div style="position:relative; bottom:400px;left:500px">
            <input type="radio" id="tampilan_belakang4_1" name="tampilan_belakang4" value="100" id="question" required>
                <label for="tampilan_belakang4_1">Casing Baik</label><br>
            <input type="radio" id="tampilan_belakang4_2" name="tampilan_belakang4" value="50" id="question">
                <label for="tampilan_belakang4_2">Casing Lecet</label><br>
            <input type="radio" id="tampilan_belakang4_3" name="tampilan_belakang4" value="0" id="question">
                <label for="tampilan_belakang4_3">Casing Retak</label><br>
            <!-- <input  type="checkbox" name="tampilan_belakang4_1" value="1">Casing Baik <br />
						<input  type="checkbox" name="tampilan_belakang4_2" value="0.5">Casing Lecet <br/>
            <input  type="checkbox" name="tampilan_belakang4_3" value="0">Casing Retak <br/> -->
    </div>
  <div class="text" style="color:black;position:relative;bottom:100px"><h4>Tampil belakang</h4></div>
  </div>

  <div class="mySlides fade">
    <div class="numbertext">3 / 3</div>
    <img src="images/kelengkapan.JPG" style="width:100%">
    <div style="position:relative; top:20px;left:50px">
            <input type="radio" id="kelengkapan1_1" name="kelengkapan1" value="100" id="question" required>
                <label for="kelengkapan1_1">Kotak Ada</label><br>
            <input type="radio" id="kelengkapan1_2" name="kelengkapan1" value="75" id="question">
                <label for="kelengkapan1_2">Kotak Tidak Ada</label><br>
						<!-- <input  type="checkbox" name="kelengkapan1_1" value="1">Kotak Ada <br />
						<input  type="checkbox" name="kelengkapan1_2" value="0">Kotak Tidak Ada <br /> -->
            </div>
      <div style="position:relative;bottom:20px;left:310px">
            <input type="radio" id="kelengkapan2_1" name="kelengkapan2" value="100" id="question" required>
                <label for="kelengkapan2_1">Baterai Baik</label><br>
            <input type="radio" id="kelengkapan2_2" name="kelengkapan2" value="0" id="question">
                <label for="kelengkapan2_2">Baterai Rusak</label><br>
            <!-- <input  type="checkbox" name="kelengkapan2_1" value="1">Baterai Baik <br />
						<input  type="checkbox" name="kelengkapan2_2" value="0">Baterai Rusak <br /> -->
      </div>
    <div style="position:relative;bottom:55px; left:490px">
            <input type="radio" id="kelengkapan3_1" name="kelengkapan3" value="100" id="question" required>
                <label for="kelengkapan3_1">Chager Baik dan Normal</label><br>
            <input type="radio" id="kelengkapan3_2" name="kelengkapan3" value="0" id="question">
                <label for="kelengkapan3_2">Chager Rusak/Hilang</label><br>
            <!-- <input  type="checkbox" name="kelengkapan3_1" value="1">Chager Baik dan Normal <br />
						<input  type="checkbox" name="kelengkapan3_2" value="0">Chager Rusak/Hilang <br/> -->
    </div>        
    <div class="text" style="color:black;"><h4>Kelengkapan</h4></div>
    <input style="position:relative;top:40px;" type="submit" name="submit" id="form_kelayakan_btn" value="Selesai"></form>
  </div>

  <!-- Next and previous buttons -->
  <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
  <a class="next" onclick="plusSlides(1)">&#10095;</a>
</div>
<br>

<!-- The dots/circles -->
<!-- <div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span>
  <span class="dot" onclick="currentSlide(2)"></span>
  <span class="dot" onclick="currentSlide(3)"></span>
</div> -->

</div>
  </div>
</div>
</section>

    <!-- FOOTER -->
    <footer id="footer" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-8">
                         <div class="footer-info">
                              <div class="section-title">
                              <h5 class="font-weight-bold text-uppercase mb-2 wow fadeInUp" data-wow-delay="0.2s" >SIMPENAN GADAI</h5>
                              <p  class="wow fadeInUp" data-wow-delay="0.2s"><em>"Sistem Pengambil Keputusan Gadai"</em></p>
                              <p  class="wow fadeInUp" data-wow-delay="0.2s">Tugas Mini Project - SPRINT Angakatan 1 2020</p>
                              </div>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-8">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2 class="wow fadeInUp" data-wow-delay="0.2s">Alamat</h2>
                              </div>
                              <ul class="list-unstyled wow fadeInUp" data-wow-delay="0.2s">
                                   <li>
                                   <p>
                                        <i class="fas fa-home mr-3"></i> Kantor Pusat: Jl. Kramat Raya 162 Jakarta Pusat 10430 </p>
                                   </li>
                                   <li>
                                   <p>
                                        <i class="fas fa-envelope mr-3"></i> pegadaian@pegadaian.co.id</p>
                                   </li>
                                   <li>
                                   <p>
                                        <i class="fas fa-phone mr-3"></i> 0213155550 | 02180635162 </p>
                                   </li>
                              </ul>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                         <br>
                         <h5 class="font-weight-bold text-uppercase mb-2 wow fadeInUp" style="padding-left:35%;" data-wow-delay="0.8s">Follow Us</h5><br>
                              <a type="button" class="btn-floating btn-fb fa-2x wow fadeInUp" style="padding-left:35%;" data-wow-delay="0.8s">
                              <i class="fab fa-facebook-f ico"></i>
                              </a> 
                              <a type="button" class="btn-floating btn-tw fa-2x wow fadeInUp" data-wow-delay="0.8s">
                                   <i class="fab fa-twitter ico"></i>
                              </a>
                              <a type="button" class="btn-floating btn-instagram fa-2x wow fadeInUp" data-wow-delay="0.8s">
                                   <i class="fab fa-instagram ico"></i>
                              </a>
                         <div class="wow fadeInUp copyright-text" data-wow-delay="0.8s"> 
                              <p style="padding-left:20%;"><br>Copyright 2020 &copy; Team4SPRINT
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>
<script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/jquery.stellar.min.js"></script>
     <script src="js/wow.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/jquery.magnific-popup.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>
  <script src="assets/js/jquery.js"></script> 
	<script src="assets/js/popper.js"></script> 
	<script src="assets/js/bootstrap.js"></script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  
<script>
	var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}
</script>

<script>
            

            var check = sessionStorage.getItem("check");
            if(check=="true"){
                var layak = document.getElementById('layak');
                layak.style.visibility='visible'
            }

            document.getElementById('column_select').onchange = function () {
                document.getElementById('type_select').setAttribute('disabled', true);  
                if(this.value.includes('mobil')){
                    var loc = "http://localhost/SIMPENAN-GADAI/MiniFormKategori/mobil.php";
                    document.getElementById('site').setAttribute('src', loc);    
                } 
                if(this.value.includes('motor')){
                    var loc = "http://localhost/SIMPENAN-GADAI/MiniFormKategori/motor.html";
                    document.getElementById('site').setAttribute('src', loc);    
                } 
                if(this.value.includes('handphone')){
                    var loc = "http://localhost/SIMPENAN-GADAI/MiniFormKategori/handphone.php";
                    document.getElementById('site').setAttribute('src', loc);    
                } 
                if(this.value.includes('laptop')){
                    var loc = "http://localhost/SIMPENAN-GADAI/MiniFormKategori/laptop.html";
                    document.getElementById('site').setAttribute('src', loc);    
                } 
            }

            document.getElementById('layout_select').onchange = function () {
                if(this.value.includes('mobil'))
                {
                    document.getElementById('type_select').removeAttribute('disabled');
                }
                else if(this.value.includes('motor'))
                {
                    document.getElementById('type_select').removeAttribute('disabled');
                }
                else if(this.value.includes('handphone'))
                {
                    document.getElementById('type_select').removeAttribute('disabled');
                }
                else if(this.value.includes('laptop'))
                {
                    document.getElementById('type_select').removeAttribute('disabled');
                }
                else
                {
                    document.getElementById('type_select').setAttribute('disabled', true);   

                }
                }  
                                            $(document).ready(function() {
                                                $("#column_select").change(function () {
                                                    $("#layout_select")
                                                        .find("option")
                                                        .show()
                                                        .not("option[value*='" + this.value + "']").hide();
                                                    $("#type_select")
                                                        .find("option")
                                                        .show()
                                                        .not("option[value*='" + this.value + "']").hide();
                                                    
                                                
                                                    $("#layout_select").val(
                                                        $("#layout_select").find("option:visible:first").val());
                                                    $("#type_select").val(
                                                        $("#type_select").find("option:visible:first").val());
                                                    
                                                }).change();
                                            });
                                            $(document).ready(function() {
                                                $("#layout_select").change(function () {
                                                    $("#type_select")
                                                        .find("option")
                                                        .show()
                                                        .not("option[value*='" + this.value + "']").hide();
                                                
                                                    $("#type_select").val(
                                                        $("#type_select").find("option:visible:first").val());
                                                    
                                                }).change();
                                            });
                                        </script>
                                        <script>
            function changeVal() {
            var elem = document.getElementById("pembayaran"),
                event = new Event('change');

            elem.value = 1;
            elem.dispatchEvent(event);
            }


            function getItems(val) {
              if(val==0){
                alert("'Mohon Maaf, Barang yang akan anda gadai tidak memenuhi syarat. Silakan ulang kembali.");
                window.location.href = 'FormKelayakan.php';}
            }

            changeVal();
     </script>
<script src="http://code.jquery.com/jquery-1.8.1.min.js"></script>
      
                                        
</body> 
</html>
