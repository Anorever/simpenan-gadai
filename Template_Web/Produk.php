<!DOCTYPE html>
    <head>
        <title>Barang Gadai</title>
        <!-- <link rel="stylesheet" type="text/css" href="css/app.css"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
 
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

        <!-- <link rel="stylesheet" href="css4.1/bootstrapcustom.min.css" crossorigin="anonymous"> -->
        <link rel="stylesheet" href="css/bootstrap-iso.css">
        <link rel="stylesheet" href="css/templatemo-style.css">

    </head>
    <body> 

    <?php 
    
    //include('header.html');

    // if($_GET["Kat"]==1){
    //     $kat = "Mobil";
    // }
    // else if($_GET["Kat"]==2){
    //   $kat = "Handphone";
    // }
    // else if($_GET["Kat"]==3){
    //   $kat = "Motor";
    // }
    // else if($_GET["Kat"]==4){
    //   $kat = "laptop";
    // }

    $kat="Handphone";
    ?>

    <div class="container text-center py-5 mt-5">
        <h1 data-aos="fade-up" style="color:green; font-size:25px; padding-bottom:0%; padding-top:10%;">Kategori Produk <strong><?php echo strtoupper($kat); ?></strong></h1>    
            <div class="container text-center mb-2 mt-2" style="border:1px solid black; padding:5px 0 5px 0;">            
              <div class="row">
                <div class="col">
                        Merek Handphone :
                          <select>
                                    <option>Samsung</option> 
                                    <option>Apple</option> 
                          </select>
                  </div>
                  <div class="col">
                        Tipe Handphone :
                          <select>
                                    <option>Samsung A7</option> 
                                    <option>Samsung A8</option> 
                          </select>
                  </div>
                  </div>
                </div>
              </div>
    </div>
   
  <div class="card" >
    <div class="card-body">    
    <div class="container">
        <div class="row align-items-center example-1 scrollbar-ripe-malinka">
            <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "images";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    // $query = 'SELECT * from images_path WHERE img_path LIKE "%'.$kat.'%" ORDER BY `Harga` ASC';
    // $result = $conn->query($query);
    // while($row = $result->fetch_array()){
    //   // echo "".$row["img_path"]."";
    //   echo " <div class='col-lg-3 col-md-6 text-center ' >
    //   <div class='card' style='width: 13rem;' >
    //   <a href='HalamanPerkiraanGadai.php?myVar=".$row["img_path"]."' onclick='post' ><img src='".$row["img_path"]."' class='img-responsive img-thumbnail cat wiggle' align='center'></a>
    //   <div class='card-body' >
    //     <p class='card-text'>".$row["Nama_Tipe"]." <br> ".$row["Harga"]."</p>
    //   </div>
    // </div>
    //   </div>
    // ";
    
    $query = 'SELECT * from images_path WHERE img_path LIKE "%'.$kat.'%" ORDER BY `Harga` ASC';
    $result = $conn->query($query);
    while($row = $result->fetch_array()){
      echo "<div class='container col-lg-3 col-md-6' align='right' style='padding-bottom:3%;'>
      <a align='right' href='HalamanPerkiraanGadai.php?myVar=".$row["img_path"]."' onclick='post' ><img src='".$row["img_path"]."' class='img-responsive img-thumbnail cat wiggle' align='center'></a>
      </div><div class='container col-lg-3 col-md-6 text-center'>
      <table>
        <tr width='200px'><td>
          <div class='card-body' align='left'>
            <b>Wilayah : Jakarta</b> <br>
            <p class='card-text'> Merek      : ".$row["Nama_Tipe"]." <br><br> Estimasi Taksir : ".$row["Harga"]."</p>
            <div align='right'>
            <button>Detail</button></div>
          </div>
        </td></tr>
      </table>
      </div>
    ";
    }
           ?>
    </div>
    <div class="container mt-5">
      <div class="row">
        <div class="col">
            <button onclick="history.go(-1);" class="btn btn-success">Ganti Kategori </button>
        </div>
        <div class="col" align="right">
        <label style="padding:2%;"> Tidak Ada Barang diatas ?  </label><a href="HalamanFormulir.php"><button class="btn btn-success">Estimasi Barang Gadai anda disini</button></a>            
        </div>
      </div>    

    </div>
    </div>
  
         <!-- FOOTER -->
         <footer id="footer" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-8">
                         <div class="footer-info">
                              <div class="section-title">
                              <h5 class="font-weight-bold text-uppercase mb-2 wow fadeInUp" data-wow-delay="0.2s" >SIMPENAN GADAI</h5>
                              <p  class="wow fadeInUp" data-wow-delay="0.2s"><em>"Sistem Pengambil Keputusan Gadai"</em></p>
                              <p  class="wow fadeInUp" data-wow-delay="0.2s">Tugas Mini Project - SPRINT Angakatan 1 2020</p>
                              </div>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-8">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2 class="wow fadeInUp" data-wow-delay="0.2s">Alamat</h2>
                              </div>
                              <ul class="list-unstyled wow fadeInUp" data-wow-delay="0.2s">
                                   <li>
                                   <p>
                                        <i class="fas fa-home mr-3"></i> Kantor Pusat: Jl. Kramat Raya 162 Jakarta Pusat 10430 </p>
                                   </li>
                                   <li>
                                   <p>
                                        <i class="fas fa-envelope mr-3"></i> pegadaian@pegadaian.co.id</p>
                                   </li>
                                   <li>
                                   <p>
                                        <i class="fas fa-phone mr-3"></i> 0213155550 | 02180635162 </p>
                                   </li>
                              </ul>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                         <br>
                         <h5 class="font-weight-bold text-uppercase mb-2 wow fadeInUp" style="padding-left:29%;" data-wow-delay="0.8s">Follow Us</h5><br>
                              <a type="button" class="btn-floating btn-fb fa-2x wow fadeInUp" style="padding-left:35%;" data-wow-delay="0.8s">
                              <i class="fab fa-facebook-f ico"></i>
                              </a> 
                              <a type="button" class="btn-floating btn-tw fa-2x wow fadeInUp" data-wow-delay="0.8s">
                                   <i class="fab fa-twitter ico"></i>
                              </a>
                              <a type="button" class="btn-floating btn-instagram fa-2x wow fadeInUp" data-wow-delay="0.8s">
                                   <i class="fab fa-instagram ico"></i>
                              </a>
                         <div class="wow fadeInUp copyright-text" data-wow-delay="0.8s"> 
                              <p style="padding-left:20%;"><br>Copyright 2020 &copy; Team4SPRINT
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>

     
     <script>
        $('.select2').select2();
     </script>
     <script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/jquery.stellar.min.js"></script>
     <script src="js/wow.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/jquery.magnific-popup.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>
     <script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
            <script>
                    $(function() {
                      $(document).scroll(function(){
                        var $nav = $("#mainNavbar");
                        $nav.toggleClass("scrolled", $(this).scrollTop() > $nav.height());
                        if($(this).scrollTop() > 0) {
                            $("nav a").css('color', 'white');
                        }
                        else{
                          $("nav a").css('color', 'green');
                        }
                      });
                      position = scroll;
                    });
                  </script>
              <script>
                  AOS.init();
                </script>
        </body>
</html>