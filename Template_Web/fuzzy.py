import numpy as np
import skfuzzy as fuzz
# from skfuzzy import control as ctrl
import skfuzzy.control as ctrl
import matplotlib.pyplot as plt
import sys

# Contoh -------------------------------------------------------------------
# x = int(sys.argv[1])
# x = sys.argv[1]
# y = sys.argv[2]
# z=x+y
# --------------------------------------------------------------------------
# --------------------------------------------------------------------------
# print("%d + %d = %d" %(x,y,z))
# print(x,y)

# New Antecedent/Consequent objects hold universe variables and membership
# functions

# casing = ctrl.Antecedent(np.arange(0, 50, 1), 'casing')
# layar = ctrl.Antecedent(np.arange(0, 50, 1), 'layar')
# tombol = ctrl.Antecedent(np.arange(0, 50, 1), 'tombol')

# --------------------------------------------------------------------------

varWarna = float(sys.argv[1])
ver_Android = float(sys.argv[2]) 
 
warna = ctrl.Antecedent(np.arange(0, 50, 1), 'warna')
versi = ctrl.Antecedent(np.arange(0, 50, 1), 'versi')

#tampilan depan
tP1 = float(sys.argv[3])
tP2 = float(sys.argv[4])
tP3 = float(sys.argv[5]) 

# print(tP1,tP2,tP3)
lampuIndikator = ctrl.Antecedent(np.arange(0, 50, 1), 'lampuIndikator')
port_usb = ctrl.Antecedent(np.arange(0, 50, 1), 'port_usb')
layar = ctrl.Antecedent(np.arange(0, 50, 1), 'layar')

#tampilan belakang
tB1 = float(sys.argv[6])
tB2 = float(sys.argv[7])
tB3 = float(sys.argv[8]) 
tB4 = float(sys.argv[9])
bt = float(sys.argv[10])
cg = float(sys.argv[11])

# print(tB1,tB2,tB3,tB4)
kamera = ctrl.Antecedent(np.arange(0, 50, 1), 'kamera')
tombol = ctrl.Antecedent(np.arange(0, 50, 1), 'tombol')
lampuFlash = ctrl.Antecedent(np.arange(0, 50, 1), 'lampuFlash')
casing = ctrl.Antecedent(np.arange(0, 50, 1), 'casing')
baterai = ctrl.Antecedent(np.arange(0, 50, 1), 'baterai')
charger = ctrl.Antecedent(np.arange(0, 50, 1), 'charger')

persentase = ctrl.Consequent(np.arange(0, 200, 1), 'persentase')


# ---------------------------------------------------------------------------

# Auto-membership function population is possible with .automf(3, 5, or 7)
warna.automf(3)
versi.automf(3)
lampuIndikator.automf(3)
port_usb.automf(3)
layar.automf(3)
kamera.automf(3)
tombol.automf(3)
lampuFlash.automf(3)
casing.automf(3)
baterai.automf(3)
charger.automf(3)

# Custom membership functions can be built interactively with a familiar,
# Pythonic API
persentase['low'] = fuzz.trimf(persentase.universe, [0, 25, 50])
persentase['medium'] = fuzz.trimf(persentase.universe, [45, 60, 80])
persentase['high'] = fuzz.trimf(persentase.universe, [75, 100, 120])

rule1 = ctrl.Rule(versi['poor'] | lampuIndikator['poor'] | port_usb['poor'] | layar['poor'] | kamera['poor'] | tombol['poor'] | lampuFlash['poor'] | casing['poor'] | baterai['poor'] | charger['poor'], persentase['low'])
rule2 = ctrl.Rule(warna['good'] & versi['good'] & lampuIndikator['good'] & port_usb['good'] & layar['good'] & kamera['good'] & tombol['good'] & lampuFlash['good'] & casing['good'] & charger['poor'] & (lampuIndikator['poor'] | port_usb['poor'] | kamera['poor'] | tombol['poor'] | baterai['poor'] | charger['poor'] | versi['poor']), persentase['low'])
rule3 = ctrl.Rule(warna['good'] & versi['good'] & lampuIndikator['good'] & port_usb['good'] & layar['good'] & kamera['good'] & tombol['good'] & lampuFlash['good'] & casing['good'] & baterai['good'] & charger['good'] & lampuIndikator['good'] & port_usb['good'] & kamera['good'] & tombol['good'], persentase['high'])
rule4 = ctrl.Rule(lampuIndikator['average'] | casing['average'] | layar['average'], persentase['medium'])

persentase_ctrl = ctrl.ControlSystem([rule1, rule2,rule3,rule4])
persentase_res = ctrl.ControlSystemSimulation(persentase_ctrl)
# Pass inputs to the ControlSystem using Antecedent labels with Pythonic API
# Note: if you like passing many inputs all at once, use .inputs(dict_of_data)

persentase_res.input['warna'] = varWarna
persentase_res.input['versi'] = ver_Android
persentase_res.input['lampuIndikator'] = tP1
persentase_res.input['port_usb'] = tP2
persentase_res.input['layar'] = tP3
persentase_res.input['kamera'] = tB1
persentase_res.input['tombol'] = tB2
persentase_res.input['lampuFlash'] = tB3
persentase_res.input['casing'] = tB4
persentase_res.input['baterai'] = bt
persentase_res.input['charger'] = cg

# Crunch the numbers
persentase_res.compute()

print(persentase_res.output['persentase'])