<!DOCTYPE html>
<html lang="en">
<!-- CSS -->
<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

/* Button used to open the contact form - fixed at the bottom of the page */
.btTxtsubmit {
  cursor: pointer;
  opacity: 0.6;
  position: fixed;
  bottom: 23px;
  right: 28px;
  width: 100px;
}

/* The popup form - hidden by default */
.form-popup {
  display: none;
  position: fixed;
  bottom: 0;
  right: 15px;
  border: 3px solid #f1f1f1;
  z-index: 9;
}

/* Add styles to the form container */
.form-container {
  max-width: 300px;
  padding: 10px;
  background-color: white;
}

/* Full-width input fields */
.form-container input[type=text], .form-container input[type=email], .form-container input[type=number] {
  width: 100%;
  padding: 15px;
  font-size: 10px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}

/* When the inputs get focus, do something */
.form-container input[type=text]:focus, .form-container input[type=email]:focus, .form-container input[type=number]:focus {
  background-color: #ddd;
  outline: none;
}

/* Set a style for the submit/close button */
.form-container .btn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 16px;
  border: none;
  cursor: pointer;
  font-size: 10px;
  width: 100%;
  margin-bottom:10px;
  opacity: 0.8;
}

/* Add a red background color to the cancel button */
.form-container .cancel {
  background-color: red;
  font-size: 10px;
}

/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}
</style>
<head>

     <title>SIMPENAN GADAI</title>

     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

     <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/font-awesome.min.css">
     <link rel="stylesheet" href="css/animate.css">
     <link rel="stylesheet" href="css/owl.carousel.css">
     <link rel="stylesheet" href="css/owl.theme.default.min.css">
     <link rel="stylesheet" href="css/magnific-popup.css">

     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
     <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

     <!-- MAIN CSS -->
     <link rel="stylesheet" href="css/templatemo-style.css">
     <link rel="stylesheet" href="css/snk.css">

</head>
<body>

     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>
               
          </div>
     </section>


     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                         <img src="images/Header_Pegadaian.png" height="8%" width="18%">
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                         <li><a href="#home" class="smoothScroll">Home</a></li>
                         <li><a href="#contact" class="smoothScroll">Lokasi</a></li>
                         <li><a href="#one" class="smoothScroll">Syarat dan Ketentuan</a></li>
                         <li><a href="#bantuan" class="smoothScroll">Bantuan</a></li>
                         <li><a href="#tentangkami" class="smoothScroll">Tentang Kami</a></li>           
                    </ul>
               </div>

          </div>
     </section>


     <!-- HOME -->
     <section id="home" class="slider" data-stellar-background-ratio="2">
          <div class="row">
               <div class="owl-carousel owl-theme">
                         <div class="item item-second">
                              <div class="caption">
                                   <div class="container">
                                        <div class="col-md-8 col-sm-12">
                                             <h3>Estimasi harga barang gadaimu!</h3>
                                             <h1>SIMPENAN GADAI</h1>
                                             <a href="#goto" class="section-btn btn btn-default smoothScroll">Simulasi</a>
                                        </div>
                                   </div>
                              </div>
                         </div>
               
               
                         <div class="item item-first">
                              <div class="caption">
                                   <!-- <div class="container text-center"> -->
                                   <div class="container">
                                   <div class="col-md-8 col-sm-12">
                                             <h3>Estimasi harga brang gadaimu!</h3>
                                             <h1>SIMPENAN GADAI</h1>
                                             <a href="#goto" class="section-btn btn btn-default smoothScroll">Simulasi</a>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>

          </div>
     </section>


     <!-- ABOUT -->
     <section id="about" data-stellar-background-ratio="0.5" style="padding-bottom:10%; padding-top:10%;">
          <div class="container">
               <div class="row">

                    <div class="col-md-6 col-sm-12">
                         <div class="wow fadeInUp about-image" data-wow-delay="0.6s">
                              <img src="images/Logo_SG.png" class="img-responsive" alt="" style="padding-top:15%;">
                         </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                         <div class="about-info">
                              <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                                   <h4>Sistem Pengambil keputusan Gadai</h4>
                                   <h2>Deskripsi dan Fitur-fitur SIMPENAN GADAI</h2>
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.4s">
                                   <ol>
                                        <li><p>Akses booking online dan simulasi gadai dapat digunakan dimana saja dan kapan saja</p></li>
                                        <li><p>Menyediakan daftar Produk elektronik dengan range nominal yang anda butuhkan serta melakukan estimasi dari barang yang akan anda gadai</p></li>
                                        <li><p>Booking online yang dapat mempermudah transaksi gadai anda berdasarkan lokasi outlet terdekat atau pilihan outlet yang akan dituju</p></li>
                                   </ol>
                              </div>
                         </div>
                    </div>
                    
               </div>
          </div>
     </section>

     <section id="goto" data-stellar-background-ratio="0.5" style="padding-bottom:15%;">
               <div class="caption">
                         <div class="container text-center">
                                  <div class="col-lg-12 col-md-8 col-sm-12">
                                     <h1 style="color :rgb(12, 131, 12); padding-top:5%; padding-bottom:3%;" align = "center" class="display-3"><image src="images/br_Logo.png" style="width:70%;"></h1>
                                     <form  class="form-inline" name="form" action="HalamanProduk.php" method="post">
                                        <table align="center" >
                                            <tr><td>
                                                  <form>
                                                       <select id="wilayah" name="wilayah" class="form-control select2 section-btn btn btn-defaultg" style="width:250px; height:100px;" >
                                                            <option value="wilayah">Wilayah</option> 
                                                            <option value="SUMATERA UTARA">SUMATERA UTARA</option>
                                                            <option value="KEPULAUAN RIAU">KEPULAUAN RIAU</option>
                                                            <option value="RIAU">RIAU</option>
                                                            <option value="SUMATERA BARAT">SUMATERA BARAT</option>
                                                            <option value="JAMBI">JAMBI</option>
                                                            <option value="SUMATERA SELATAN">SUMATERA SELATAN</option>
                                                            <option value="BANGKA BELITUNG">BANGKA BELITUNG</option>
                                                            <option value="LAMPUNG">LAMPUNG</option>
                                                            <option value="BENGKULU">BENGKULU</option>
                                                            <option value="KALIMANTAN SELATAN">KALIMANTAN SELATAN</option>
                                                            <option value="KALIMANTAN TIMUR">KALIMANTAN TIMUR</option>
                                                            <option value="KALIMANTAN TENGAH">KALIMANTAN TENGAH</option>
                                                            <option value="KALIMANTAN BARAT">KALIMANTAN BARAT</option>
                                                            <option value="SULAWESI SELATAN">SULAWESI SELATAN</option>
                                                            <option value="MALUKU">MALUKU</option>
                                                            <option value="SULAWESI TENGGARA">SULAWESI TENGGARA</option>
                                                            <option value="SULAWESI BARAT">SULAWESI BARAT</option>
                                                            <option value="SULAWESI UTARA">SULAWESI UTARA</option>
                                                            <option value="GORONTALO">GORONTALO</option>
                                                            <option value="MALUKU UTARA">MALUKU UTARA</option>
                                                            <option value="SULAWESI TENGAH">SULAWESI TENGAH</option>
                                                            <option value="PAPUA BARAT">PAPUA BARAT</option>
                                                            <option value="PAPUA">PAPUA</option>
                                                            <option value="BALI">BALI</option>
                                                            <option value="NUSA TENGGARA BARAT (NTB)">NUSA TENGGARA BARAT (NTB)</option>
                                                            <option value="NUSA TENGGARA TIMUR (NTT)">NUSA TENGGARA TIMUR (NTT)</option>
                                                            <option value="DKI JAKARTA">DKI JAKARTA</option>
                                                            <option value="JAWA BARAT">JAWA BARAT</option>
                                                            <option value="BANTEN">BANTEN</option>
                                                            <option value="JAWA TENGAH">JAWA TENGAH</option>
                                                            <option value="DI YOGYAKARTA">DI YOGYAKARTA</option>
                                                            <option value="JAWA TIMUR">JAWA TIMUR</option>
                                                            <option value="NANGGROE ACEH DARUSSALAM (NAD)">NANGGROE ACEH DARUSSALAM (NAD)</option>
                                                            </select>
                                                       </form>
                                                  </td>
                                                  <td style="padding-left:5%; width:15cm;" >
                                                       <input style="width: 100%; padding-left:3%; font-size:15px;" class="form-control mr-sm-2" type="text" name="nominal" id="nominal" placeholder="Masukkan jumlah uang yang Anda perlukan" aria-label="Search">
                                                  </td><td style="padding-left:3%;">     
                                                       <a href="#" onclick="return checkInp();" class="btn btn-primary btn-lg" role="button" style="background-color: green;"><i class="fas fa-search"></i></a>                  
                                                  </td></tr>
                                             </table>
                                      </form>
                                        </div>
                                   </div>
                              </div>     
     </section>
 
     <p style="padding-top:8%;">-</p>
     <section id="one" class="wrapper">
                    <h2 class="text-center">SYARAT DAN KETENTUAN</h2>
				<div class="inner flex flex-3">
					<div class="flex-item left">
                              <div>
							<h3>Kartu Identitas</h3>
							<p>Menyerahkan Fotokopi<br>KTP atau kartu Identitas lainnya
						</div>
						<!-- <div>
							<h3>Kartu Identitas</h3>
							<p>Fotokopi KTP atau<br /> identitas resmi lainnya</p>
						</div> -->
						<div>
							<h3>Barang Jaminan</h3>
							<p>Menyerahkan barang jaminan<br /> yang akan digadai</p>
						</div>
					</div>
					<div class="flex-item image fit round">
						<img src="images/pic01.jpg" alt="" />
					</div>
					<div class="flex-item right">
						<div>
							<h3>(Khusus) Kendaraan Bermotor </h3>
							<p>Untuk kendaraan bermotor <br /> membawa BPKB dan STNK asli</p>
						</div>
						<div>
							<h3>Surat Bukti Kredit (SKB)</h3>
							<p>Nasabah menandatangani Surat Bukti Kredit (SKB)</p>
						</div>
					</div>
				</div>
               </section>

     <p style="padding-top:8%;">-</p>
     <section id="bantuan" data-stellar-background-ratio="0.5">
               <div class="row">
                    <div class="col-md-6 col-sm-12" >
                         <div class="container text-center" style="padding-left:35%;">
                              <div class="wow fadeInUp about-image" data-wow-delay="0.6s">
                                   <img src="images/faq.jpg" class="img-responsive text-center" alt="" style="padding-top:30%;">
                              </div>
                         </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                         <div class="container" align="left">
                              <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                                   <h1> FAQ </h1>
                                   <h5>Pertanyaan yang sering diajukan</h5>
                              </div>
                              <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                                   <a data-toggle="collapse" href="#footwear" aria-expanded="false" aria-controls="footwear">1. Bagaimana tata cara menggadaikan barang?</a>
                                   <div class="collapse" id="footwear">
                                   <p> <li> Nasabah datang dengan membawa barang jaminan dan kartu identitas diri </li>
                                        <li> Barang jaminan ditaksir oleh penaksir </li>
                                        <li> Uang pinjaman diterima oleh nasabah dalam bentuk tunai atau transfer</li></p>
                                   </div>
                              </div>
                              <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                                   <a data-toggle="collapse" href="#footwear2" aria-expanded="false" aria-controls="footwear2">2. Dimana lokasi pegadaian terdekat?</a>
                                   <div class="collapse" id="footwear2">
                                   <p><li><a href="#contact" class="smoothScroll">Klik Disini!</a></li></p>
                                   </div>
                              </div>
                              <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                                   <a data-toggle="collapse" href="#footwear3" aria-expanded="false" aria-controls="footwear3">3. Apa saja produk-produk pegadaian?</a>
                                   <div class="collapse" id="footwear3">
                                         <p><li><a href="https://sahabatpegadaian.com/produk-pegadaian" class="smoothScroll">Klik Disini!</a></li></p>
                                   </div>
                              </div>
                              <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                                   <a data-toggle="collapse" href="#footwear4" aria-expanded="false" aria-controls="footwear4">4. Berapa lama tenor yang diberikan?</a>
                                   <div class="collapse" id="footwear4">
                                   <p><li>Pinjaman dengan jangka waktu 4 (empat) bulan dan dapat diperpanjang berkali-kali</li></p>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
     </section>

     <!-- CONTACT -->
     <section id="contact" data-stellar-background-ratio="0.5" style="padding-top:5%;">
          <div class="container">
               <div class="row">
                    <div class="wow fadeInUp col-md-12 col-sm-12" data-wow-delay="0.4s">                        
                         <div id="google-map">
                              <h3 class="text-center" style="padding-bottom:2%;"> Outlet Pegadaian Terdekat </h3>
                              <iframe src="map.html"  style="padding-bottom:2%;"></iframe>
                       </div>
                    </div>    
               </div>
          </div>
     </section>          

     <!-- Chat Bot -->
<!--      
     <iframe
          //allow="microphone;"
          width="350"
          height="430"
          src="https://console.dialogflow.com/api-client/demo/embedded/2d892b56-6acb-45b7-acae-1e57e811ae90">
     </iframe> -->

     <!-- FOOTER -->
     <footer id="tentangkami" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-8">
                         <div class="footer-info">
                              <div class="section-title">
                              <h5 class="font-weight-bold text-uppercase mb-2 wow fadeInUp" data-wow-delay="0.2s" >SIMPENAN GADAI</h5>
                              <p  class="wow fadeInUp" data-wow-delay="0.2s"><em>"Sistem Pengambil Keputusan Gadai"</em></p>
                              <p  class="wow fadeInUp" data-wow-delay="0.2s">Tugas Mini Project - SPRINT Angakatan 1 2020</p>
                              </div>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-8">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h5 class="wow fadeInUp" data-wow-delay="0.2s">Hubungi Kami</h5>
                              </div>
                              <ul class="list-unstyled wow fadeInUp" data-wow-delay="0.2s">
                                   <li>
                                   <p>
                                        <i class="fas fa-home mr-3"></i> Kantor Pusat: Jl. Kramat Raya 162 Jakarta Pusat 10430 </p>
                                   </li>
                                   <li>
                                   <p>
                                        <i class="fas fa-envelope mr-3"></i> pegadaian@pegadaian.co.id</p>
                                   </li>
                                   <li>
                                   <p>
                                        <i class="fas fa-phone mr-3"></i> 0213155550 | 02180635162 </p>
                                   </li>
                              </ul>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                         <br>
                         <h5 class="font-weight-bold text-uppercase mb-2 wow fadeInUp" style="padding-left:29%;" data-wow-delay="0.8s">Follow Us</h5><br>
                              <a href="https://www.facebook.com/PegadaianPersero/" type="button" class="btn-floating btn-fb fa-2x wow fadeInUp" style="padding-left:30%;" data-wow-delay="0.8s">
                              <i class="fab fa-facebook-f ico"></i>
                              </a> 
                              <a href="https://twitter.com/Pegadaian" type="button" class="btn-floating btn-tw fa-2x wow fadeInUp" data-wow-delay="0.8s">
                                   <i class="fab fa-twitter ico"></i>
                              </a>
                              <a href="https://www.instagram.com/pegadaian_id/?hl=en" type="button" class="btn-floating btn-instagram fa-2x wow fadeInUp" data-wow-delay="0.8s">
                                   <i class="fab fa-instagram ico"></i>
                              </a>
                         <div class="wow fadeInUp copyright-text" data-wow-delay="0.8s"> 
                              <p style="padding-left:10%;"><br>Copyright 2020 &copy; Team4SPRINT
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>

     <!-- PopUp Menu Kontak Kami -->
     <input type="image" src="images/contact-37-512.png" name="saveForm" class="btTxtsubmit" onclick="openForm()" id="saveForm" />

     <div class="form-popup" id="myForm">
     <form action="/action_page.php" class="form-container">
     <h5 align="center">Kontak Kami</h5>

     <label for="email"><b>Nama</b></label>
     <input type="text" placeholder="Masukan Nama" name="nama" required>

     <label for="email"><b>Email</b></label>
     <input type="email" placeholder="Masukan Email" name="email" required>

     <label for="nohp"><b>No Handphone</b></label>
     <input type="number" placeholder="Masukan No HP" name="nohp" required>

     <div >
          <input type="radio" id="pertanyaan" name="pertanyaan" value="pengajuangadai" id="question" required>
                <label for="pertanyaan">Pengajuan Gadai</label>
          <input type="radio" id="pertanyaan" name="pertanyaan" value="konsultasi" id="question">
                <label for="pertanyaan">Konsultasi</label><br>
          <input type="radio" id="pertanyaan" name="pertanyaan" value="masukanusulan" id="question">
                <label for="pertanyaan">Masukan/Usulan</label>
          <input type="radio" id="pertanyaan" name="pertanyaan" value="kendalateknis" id="question">
                <label for="pertanyaan">Kendala Teknis</label><br>
          <input type="radio" id="pertanyaan" name="pertanyaan" value="lainnya" id="question">
                <label for="pertanyaan">Lainnya</label><br>
      </div>

     <button type="submit" class="btn">Submit</button>
     <button type="button" class="btn cancel" onclick="closeForm()">Close</button>
     </form>
     </div>


     <!-- SCRIPTS -->

     <script>
        $('.select2').select2();
     </script>
     <script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/jquery.stellar.min.js"></script>
     <script src="js/wow.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/jquery.magnific-popup.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>
     <script>
          function checkInp()
          {    
               var ddl = document.getElementById("wilayah");
               var selectedValue = ddl.options[ddl.selectedIndex].value;
               if(selectedValue=="wilayah"){
                    Swal.fire({
                    title: 'Masukkan Lokasi anda!',
                    icon: 'warning',
                    })
                    return false;
               }
               var x=document.forms["form"]["nominal"].value;
               if(isNaN(x) || x.includes(".")){
                    Swal.fire({
                    title: 'Inputan yang anda masukan tidak valid!',
                    icon: 'warning',
                    })
                    return false;
               }
               else if (x == "")
               {
                    Swal.fire({
                    title: 'Inputan tidak boleh kosong!',
                    icon: 'warning',
                    })
                    return false;
               }
               else if(x%10000!=0){
                    Swal.fire({
                    title: 'Inputan harus merupakan kelipatan 10000!',
                    icon: 'warning',
                    })
                    return false;
               }
               else if(x<100000){
                    Swal.fire({
                    title: 'Inputan harus lebih dari Rp 100.000!',
                    icon: 'warning',
                    })
                    return false;
               }
               else if(x>10000000){
                    Swal.fire({
                    title: 'Inputan max Rp 10.000.000!',
                    icon: 'warning',
                    })
                    return false;
               }
               else{
                    document.forms[0].submit();return true;
               }
          }

    </script>

    <script src="sweetalert2.all.min.js"></script>
     <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
     <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

     <script>
     function openForm() {
          document.getElementById("myForm").style.display = "block";
     }

     function closeForm() {
          document.getElementById("myForm").style.display = "none";
     }
     </script>


</body>
</html>