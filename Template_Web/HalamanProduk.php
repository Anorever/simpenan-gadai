<!DOCTYPE html>
    <head>
        <title>Barang Gadai</title>
        <!-- <link rel="stylesheet" type="text/css" href="css/app.css"> -->
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
 
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

        <!-- <link rel="stylesheet" href="css4.1/bootstrapcustom.min.css" crossorigin="anonymous"> -->
        <link rel="stylesheet" href="css/bootstrap-iso.css">
        <link rel="stylesheet" href="css/templatemo-style2.css">

    </head>
    <body> 

     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                         <img src="images/Header_Pegadaian.png" height="8%" width="18%">
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                         <li><a href="index.php#home" class="smoothScroll">Home</a></li>
                         <li><a href="index.php#contact" class="smoothScroll">Lokasi</a></li>
                         <li><a href="index.php#team" class="smoothScroll">Syarat dan Ketentuan</a></li>
                         <li><a href="index.php#bantuan" class="smoothScroll">Bantuan</a></li>
                         <li><a href="index.php#tentangkami" class="smoothScroll">Tentang Kami</a></li>                              
                         <!--<a href="#footer" class="section-btn">Login</a>-->
                    </ul>
               </div>

          </div>
     </section>


    <?php 

//     $wilayah = $_GET["wilayah"];
    
    //include('header.html');

    // if($_GET["Kat"]==1){
    //     $kat = "Mobil";
    // }
    // else if($_GET["Kat"]==2){
    //   $kat = "Handphone";
    // }
    // else if($_GET["Kat"]==3){
    //   $kat = "Motor";
    // }
    // else if($_GET["Kat"]==4){
    //   $kat = "laptop";
    // }

    $kat="Handphone";
    ?>
    
    <div class="container text-center py-5 mt-5" >
        <h5 style="color:black; font-size:25px; padding-bottom:0%; padding-top:10%;">Kategori Produk <strong><?php echo strtoupper($kat); ?></strong></h5>
        <hr border="2">       
              <div class="row">
                    <div class="col-lg-5" style="padding-left:200px;"> 

                         <form method="post" action="">
                         <!-- <select name="change">
                         <option value="oppo">Maintenance On</option>
                         <option value="oppo">Maintenance Off</option>
                         </select> -->
                         <!-- <input type="text" name="nominal" value="<?=$_POST['nominal']?>" hidden> -->
                         <!-- <input type="text" name="wilayah" value="<?=$_POST['wilayah']?>" hidden> -->
                         <!-- <input type="submit" name="update" value="update"> -->
                         <!-- </form>
                         <form  class="form-inline" name="form" action="<?=$_SERVER['PHP_SELF']?>" method="get"> -->
                                        <table align="center" >
                                            <tr><td>
                                                  <form>
                                                       <select value="<?=$_POST['wilayah']?>" id="wilayah" name="wilayah" class="form-control select2 btn-primary btn-lg" style="width:250px; height:100px;" >
                                                            <option value="wilayah">Wilayah</option> 
                                                            <option value="SUMATERA UTARA">SUMATERA UTARA</option>
                                                            <option value="KEPULAUAN RIAU">KEPULAUAN RIAU</option>
                                                            <option value="RIAU">RIAU</option>
                                                            <option value="SUMATERA BARAT">SUMATERA BARAT</option>
                                                            <option value="JAMBI">JAMBI</option>
                                                            <option value="SUMATERA SELATAN">SUMATERA SELATAN</option>
                                                            <option value="BANGKA BELITUNG">BANGKA BELITUNG</option>
                                                            <option value="LAMPUNG">LAMPUNG</option>
                                                            <option value="BENGKULU">BENGKULU</option>
                                                            <option value="KALIMANTAN SELATAN">KALIMANTAN SELATAN</option>
                                                            <option value="KALIMANTAN TIMUR">KALIMANTAN TIMUR</option>
                                                            <option value="KALIMANTAN TENGAH">KALIMANTAN TENGAH</option>
                                                            <option value="KALIMANTAN BARAT">KALIMANTAN BARAT</option>
                                                            <option value="SULAWESI SELATAN">SULAWESI SELATAN</option>
                                                            <option value="MALUKU">MALUKU</option>
                                                            <option value="SULAWESI TENGGARA">SULAWESI TENGGARA</option>
                                                            <option value="SULAWESI BARAT">SULAWESI BARAT</option>
                                                            <option value="SULAWESI UTARA">SULAWESI UTARA</option>
                                                            <option value="GORONTALO">GORONTALO</option>
                                                            <option value="MALUKU UTARA">MALUKU UTARA</option>
                                                            <option value="SULAWESI TENGAH">SULAWESI TENGAH</option>
                                                            <option value="PAPUA BARAT">PAPUA BARAT</option>
                                                            <option value="PAPUA">PAPUA</option>
                                                            <option value="BALI">BALI</option>
                                                            <option value="NUSA TENGGARA BARAT (NTB)">NUSA TENGGARA BARAT (NTB)</option>
                                                            <option value="NUSA TENGGARA TIMUR (NTT)">NUSA TENGGARA TIMUR (NTT)</option>
                                                            <option value="DKI JAKARTA">DKI JAKARTA</option>
                                                            <option value="JAWA BARAT">JAWA BARAT</option>
                                                            <option value="BANTEN">BANTEN</option>
                                                            <option value="JAWA TENGAH">JAWA TENGAH</option>
                                                            <option value="DI YOGYAKARTA">DI YOGYAKARTA</option>
                                                            <option value="JAWA TIMUR">JAWA TIMUR</option>
                                                            <option value="NANGGROE ACEH DARUSSALAM (NAD)">NANGGROE ACEH DARUSSALAM (NAD)</option>
                                                            </select>
                                                  </td>
                                                  <td style="padding-left:5%; width:15cm;" >
                                                       <input style="width: 450px; padding-left:3%; font-size:15px;" class="form-control mr-sm-2" type="text" name="nominal" id="nominal" placeholder="Masukkan jumlah uang yang Anda perlukan" aria-label="Search" value="<?=$_POST['nominal']?>">
                                                  </td><td style="padding-left:10%;">     
                                                       <button name="update" value="update" type="submit" onclick="return checkInp();" class="btn btn-primary btn-lg" role="button" style="background-color: green;"><i class="fas fa-search"></i><button>                  
                                                  </td></tr>
                                             </table>
                                   </div>
                              </div>
                                 
            <td></br></td>
                <div align="center" class="">
                        <label> Merek Handphone </label>
                        <select name="layout_select column_select" id="layout_select">
                                  <option value="handphone-1">Samsung</option>
                                  <option value="handphone-2">Iphone</option>
                                  <option value="handphone-3">Xiaomi</option>
                                  <option value="handphone-4">Oppo</option>
                                  <option value="handphone-5">Vivo</option>    
                          </select>
                  
    
                        <label style="padding-left:10%;">Tipe Handphone</label> 
                        <select name="type_select"  id="type_select" disable>
                            <<option value="handphone-1-1Samsung galaxy A30S">Samsung A30S</option>
                            <option value="handphone-1-2Samsung galaxy A50S">Samsung A50S</option>
                            <option value="handphone-1-3Samsung galaxy A9">Samsung A9</option>
                            <option value="handphone-1-4Samsung galaxy A50">Samsung A50</option>
                            <option value="handphone-1-5Samsung galaxy A80">Samsung A80</option>

                            <option value="handphone-2-1Iphone X 64GB">Iphone X 64GB</option>
                            <option value="handphone-2-2Iphone 8+ 64GB">Iphone 8+ 64GB</option>
                            <option value="handphone-2-3Iphone 8 64GB<">Iphone 8 64GB</option>
                            <option value="handphone-2-4Iphone 7+">Iphone 7+</option>
                            <option value="handphone-2-5Iphone 7 64GB">Iphone 7 64GB</option>

                            <option value="handphone-3-1Xiaomi Redmi 7A">Xiaomi Redmi 7A</option>
                            <option value="handphone-3-2Xiaomi 5A">Xiaomi 5A</option>
                            <option value="handphone-3-3Xiaomi Note 8">Xiaomi Note 8</option>
                            <option value="handphone-3-4Xiaomi Mi A1">Xiaomi Mi A1</option>
                            <option value="handphone-3-5Xiaomi Mi 8">Xiaomi Mi 8</option>

                            <option value="handphone-4-1Oppo F7">Oppo F7</option>
                            <option value="handphone-4-2Oppo F9">Oppo F9</option>
                            <option value="handphone-4-3Oppo F11">Oppo F11</option>
                            <option value="handphone-4-4Oppo A5S">Oppo A5S</option>
                            <option value="handphone-4-5Oppo A3S">Oppo A3S</option>

                            <option value="handphone-5-1Vivo Y95">Vivo Y95</option>
                            <option value="handphone-5-2Vivo Y15">Vivo Y15</option>
                            <option value="handphone-5-3Vivo S1">Vivo S1</option>
                            <option value="handphone-5-4Vivo Z1 pro">Vivo Z1 pro</option>
                            <option value="handphone-5-5Vivo V17 pro">Vivo V17 pro</option>
                          </select>
                  </div>
                  </div>
                </div>
              
    </div>
    </form>
    <hr border="2">
   <br><br>
  <div class="card" >
    <div class="card-body" >    
    <div class="container">
        <div class="">
            <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "images";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $dbname2 = "sim-gade";
    
    // Create connection
    $conn2 = new mysqli($servername, $username, $password, $dbname2);
    // Check connection
    if ($conn2->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    // insert into database
     $sql = 'INSERT INTO halaman_utama_input (wilayah, nominal) VALUES
     ("'.$_POST['wilayah'].'", '.$_POST['nominal'].')';

     mysqli_query($conn2, $sql);

     $query = 'SELECT * from images_path WHERE img_path LIKE "%'.$kat.'%" ORDER BY `Harga` ASC';
     $wilayah=$_POST['wilayah'];
     $nominal=$_POST['nominal'];
     
     if(isset($_POST['update'])){
          $tipe = substr($_POST['type_select'],13);
          // $value = $_POST['change'];
          $wilayah=$_POST['wilayah'];
          $nominal=$_POST['nominal'];
          $query = 'SELECT * from images_path WHERE img_path LIKE "%'.$tipe.'%" ORDER BY `Harga` ASC';
     }
 
    $result2 = $conn->query("SELECT * FROM  images_path WHERE Harga > ".$_POST['nominal']." AND img_path LIKE '%handphone%'");
    $result = $conn->query($query);
    while($row = $result->fetch_array()){
      if($row['Harga']>$nominal){
      $count=($row['Harga']/2)+($row['Harga']/3);
      $estimated="Rp.".(int)$count." - Rp.".$row['Harga'];
      
      echo "<div class='container col-lg-4 col-md-6' align='center' style='padding-bottom:3%;' >
      <a style='padding-left:30%;' align='right' href='HalamanPerkiraanGadai.php?myVar=".$row["img_path"]."' onclick='post' ><img src='".$row["img_path"]."' class='img-responsive img-thumbnail cat wiggle' align='center'></a>
      </div><div class='container col-lg-2 col-md-6'>
      <form method='POST' action='HalamanEstimasi.php' name='frm'>
      <table>
        <tr width='200px'><td>
          <div class='card-body' align='left' style='padding-top:15%;'> 
            <input value='".$row["img_path"]."' name='image_path' hidden>
            <input value='".$wilayah."' name='area' hidden>
              <b>Wilayah : '".$wilayah."'</b> <br>
              <p class='card-text'>Merek  :<input value='".$row["Nama_Tipe"]."' name='tipe' readonly style='border:none !important; background-color:transparent;'>
              <br><br> Estimasi Taksir : <br> <input value='".$estimated."' name='range_harga' readonly style='border:none !important; background-color:transparent;'></p> 
            
            <div align='right'>
                <input type='submit' name='submit' href='HalamanEstimasi.php' class='btn btn-default' value='Detail'>
                   
            </form>
          </div>
        </td></tr>
      </table>
      </div>
    ";
      }}
      if(!$result2||mysqli_num_rows($result2) == 0){
          echo"<script type='text/javascript'>
          Swal.fire({
                      title: 'Barang tidak tersedia',
                      text: 'Silakan coba jumlah nominal lain!',
                      icon: 'error',
                      confirmButtonColor: '#3085d6',
                      confirmButtonText: 'OK'
                    }).then((result) => {
                      if (result.value) {
                            window.location = 'index.php#goto';
                      }
                    })
          
      </script>";
     }
           ?>
    </div></div>
    <div class="container mt-5">
      <div class="row">
        <div class="col" align="right" style="padding-right:4%;">
        <label style="padding:2%;"> Tidak Ada Barang diatas ?  </label><a href="FormKelayakan.php"><button class="btn btn-success">Estimasi Barang Gadai anda disini</button></a>            
        </div>
      </div>    

    </div>
    </div>
  
         <!-- FOOTER -->
         <footer id="footer" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-8">
                         <div class="footer-info">
                              <div class="section-title">
                              <h5 class="font-weight-bold text-uppercase mb-2 wow fadeInUp" data-wow-delay="0.2s" >SIMPENAN GADAI</h5>
                              <p  class="wow fadeInUp" data-wow-delay="0.2s"><em>"Sistem Pengambil Keputusan Gadai"</em></p>
                              <p  class="wow fadeInUp" data-wow-delay="0.2s">Tugas Mini Project - SPRINT Angakatan 1 2020</p>
                              </div>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-8">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2 class="wow fadeInUp" data-wow-delay="0.2s">Alamat</h2>
                              </div>
                              <ul class="list-unstyled wow fadeInUp" data-wow-delay="0.2s">
                                   <li>
                                   <p>
                                        <i class="fas fa-home mr-3"></i> Kantor Pusat: Jl. Kramat Raya 162 Jakarta Pusat 10430 </p>
                                   </li>
                                   <li>
                                   <p>
                                        <i class="fas fa-envelope mr-3"></i> pegadaian@pegadaian.co.id</p>
                                   </li>
                                   <li>
                                   <p>
                                        <i class="fas fa-phone mr-3"></i> 0213155550 | 02180635162 </p>
                                   </li>
                              </ul>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                         <br>
                         <h5 class="font-weight-bold text-uppercase mb-2 wow fadeInUp" style="padding-left:35%;" data-wow-delay="0.8s">Follow Us</h5><br>
                              <a type="button" class="btn-floating btn-fb fa-2x wow fadeInUp" style="padding-left:35%;" data-wow-delay="0.8s">
                              <i class="fab fa-facebook-f ico"></i>
                              </a> 
                              <a type="button" class="btn-floating btn-tw fa-2x wow fadeInUp" data-wow-delay="0.8s">
                                   <i class="fab fa-twitter ico"></i>
                              </a>
                              <a type="button" class="btn-floating btn-instagram fa-2x wow fadeInUp" data-wow-delay="0.8s">
                                   <i class="fab fa-instagram ico"></i>
                              </a>
                         <div class="wow fadeInUp copyright-text" data-wow-delay="0.8s"> 
                              <p style="padding-left:20%;"><br>Copyright 2020 &copy; Team4SPRINT
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>

     <script>
          
          $(document).ready(function() {
                                                $("#column_select").change(function () {
                                                    $("#layout_select")
                                                        .find("option")
                                                        .show()
                                                        .not("option[value*='" + this.value + "']").hide();
                                                    $("#type_select")
                                                        .find("option")
                                                        .show()
                                                        .not("option[value*='" + this.value + "']").hide();
                                                    
                                                
                                                    $("#layout_select").val(
                                                        $("#layout_select").find("option:visible:first").val());
                                                    $("#type_select").val(
                                                        $("#type_select").find("option:visible:first").val());
                                                    
                                                }).change();
                                            });
                                            $(document).ready(function() {
                                                $("#layout_select").change(function () {
                                                    $("#type_select")
                                                        .find("option")
                                                        .show()
                                                        .not("option[value*='" + this.value + "']").hide();
                                                
                                                    $("#type_select").val(
                                                        $("#type_select").find("option:visible:first").val());
                                                    
                                                }).change();
                                            });
                                        </script>
     <script>
        $('.select2').select2();
     </script>
     <script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/jquery.stellar.min.js"></script>
     <script src="js/wow.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/jquery.magnific-popup.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>
     <script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
            <script>
                    $(function() {
                      $(document).scroll(function(){
                        var $nav = $("#mainNavbar");
                        $nav.toggleClass("scrolled", $(this).scrollTop() > $nav.height());
                        if($(this).scrollTop() > 0) {
                            $("nav a").css('color', 'white');
                        }
                        else{
                          $("nav a").css('color', 'green');
                        }
                      });
                      position = scroll;
                    });
                  </script>
              <script>
                  AOS.init();
                </script>
          <script>
          function checkInp()
          {    
               var ddl = document.getElementById("wilayah");
               var selectedValue = ddl.options[ddl.selectedIndex].value;
               if(selectedValue=="wilayah"){
                    Swal.fire({
                    title: 'Masukkan Lokasi anda!',
                    icon: 'warning',
                    })
                    return false;
               }
               var x=document.forms["form"]["nominal"].value;
               if(isNaN(x) || x.includes(".")){
                    Swal.fire({
                    title: 'Inputan yang anda masukan tidak valid!',
                    icon: 'warning',
                    })
                    return false;
               }
               else if (x == "")
               {
                    Swal.fire({
                    title: 'Inputan tidak boleh kosong!',
                    icon: 'warning',
                    })
                    return false;
               }
               else if(x%10000!=0){
                    Swal.fire({
                    title: 'Inputan harus merupakan kelipatan 10000!',
                    icon: 'warning',
                    })
                    return false;
               }
               else if(x<100000){
                    Swal.fire({
                    title: 'Inputan harus lebih dari Rp 100.000!',
                    icon: 'warning',
                    })
                    return false;
               }
               else if(x>10000000){
                    Swal.fire({
                    title: 'Inputan max Rp 10.000.000!',
                    icon: 'warning',
                    })
                    return false;
               }
               else{
                    document.forms[0].submit();return true;
               }
          }

    </script>
    <script src="sweetalert2.all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>


        </body>
</html>