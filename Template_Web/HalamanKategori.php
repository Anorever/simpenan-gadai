<!DOCTYPE html>
    <head>
        <title>Kategori Barang</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/app.css">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet"> 
    </head>
    <body> 
    
    <?php include('header.html');?>

        <?php
          function rupiah($angka){
	
            $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
            return $hasil_rupiah;
           
          }
          ?>
        
        <div class="container text-center py-5 mt-4">
                <h3 id="greentext"data-aos="fade-up" style="color:green; font-size:25px;">Kategori barang dengan harga gadai <strong>± <?php echo rupiah($_GET["nominal"]); ?></strong></h3>
        </div>

        <!-- <img src="images/Handphone/Iphone 7 64GB.jpg"> -->
        <? $Kat = 1?>
        <div class="container text-center">
            <section class="container-fluid px-0 text-center">
            <div class="row align-items-center">
            <div class="col-lg-3 col-md-6 text-center" data-aos="zoom-in-up">
                <a href='HalamanProduk.php?Kat=1' ><img src="images/Kat_Icon/car.png" class="cat wiggle"></a><br><br>
                <h3 id="greentext" style="padding-top:25px !important;">Mobil</h3>
            </div>
        <?php    
            echo "<div class='col-lg-3 col-md-6 text-center' data-aos='zoom-in-up'>
                <a href='HalamanProduk.php?Kat=2&nominal=".urlencode($_GET['nominal'])."' ><img src='images/Kat_Icon/Handphone.png' class='cat wiggle'></a><br><br><br>
                <h3 id='greentext'>Handphone</h3>
            </div>";
        ?>
            <div class="col-lg-3 col-md-6 text-center " data-aos="zoom-in-up">
                <a href='HalamanProduk.php?Kat=3'><img src="images/Kat_Icon/motorcycle.png" class="cat wiggle"></a><br><br><br>
                <h3 id="greentext">Motor</h3>
            </div>
            <div class="col-lg-3 col-md-6 text-center" data-aos="zoom-in-up">
                <a href='HalamanProduk.php?Kat=4'><img src="images/Kat_Icon/Laptop.png" class="cat wiggle"></a><br><br><br>
                <h3 id="greentext">Laptop</h3>
            </div>
        </div>
        </div>
        </section>
    </div>
    
    <?php include("footer.html");?>
          

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
            <script>
                    $(function() {
                      $(document).scroll(function(){
                        var $nav = $("#mainNavbar");
                        $nav.toggleClass("scrolled", $(this).scrollTop() > $nav.height());
                        if($(this).scrollTop() > 0) {
                            $("nav a").css('color', 'white');
                        }
                        else{
                          $("nav a").css('color', 'green');
                        }
                      });
                      position = scroll;
                    });
                  </script>
              <script>
                  AOS.init();
                </script>
        </body>
</html>