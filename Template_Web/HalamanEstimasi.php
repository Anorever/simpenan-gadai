<!DOCTYPE html>
<head>
        <title>Barang Gadai</title>
        <!-- <link rel="stylesheet" type="text/css" href="css/app.css"> -->
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
 
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

        <!-- <link rel="stylesheet" href="css4.1/bootstrapcustom.min.css" crossorigin="anonymous"> -->
        <link rel="stylesheet" href="css/bootstrap-iso.css">
        <link rel="stylesheet" href="css/templatemo-style2.css">

    </head>
    
    <!-- MENU -->
    <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                         <img src="images/Header_Pegadaian.png" height="8%" width="18%">
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                         <li><a href="index.php#home" class="smoothScroll">Home</a></li>
                         <li><a href="index.php#contact" class="smoothScroll">Lokasi</a></li>
                         <li><a href="index.php#team" class="smoothScroll">Syarat dan Ketentuan</a></li>
                         <li><a href="index.php#bantuan" class="smoothScroll">Bantuan</a></li>  
                         <li><a href="index.php#tentangkami" class="smoothScroll">Tentang Kami</a></li>                                
                    </ul>
               </div>

          </div>
     </section>

      <div class="container text-center py-4 mt-5" style="padding-top:8%;">
                <h1 id="greentext" data-aos="fade-up" style="color:green; font-size:25px; padding-bottom:0%;">DETAIL PRODUK</h1>
      </div>

            <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "images";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $query = 'SELECT * from images_path where img_path="images/Handphone/samsung galaxy A30S.jpg"';
    $result = mysqli_query($conn,$query);

    $dbname2 = "sim-gade";
    
    // Create connection
    $conn2 = new mysqli($servername, $username, $password, $dbname2);
    // Check connection
    if ($conn2->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    // insert into database
     $sql = 'INSERT INTO button_detail_product (wilayah, merek,estimasi_taksir) VALUES
     ("'.$_POST['area'].'","'.$_POST['tipe'].'","'.$_POST['range_harga'].'")';

     mysqli_query($conn2, $sql);


    // $tipe = substr($_POST['tipe'],13);
    $tipe = $_POST['tipe'];
    $merek = '';
    if (strpos($tipe, 'Samsung') !== false) {
      $merek='Samsung';
    }
    else if (strpos($tipe, 'Iphone') !== false) {
      $merek='Iphone';
    }
    else if (strpos($tipe, 'Xiaomi') !== false) {
      $merek='Xiaomi';
    }
    else if (strpos($tipe, 'Oppo') !== false) {
      $merek='Oppo';
    }
    else if (strpos($tipe, 'Vivo') !== false) {
      $merek='Vivo';
    }
    $row = mysqli_fetch_assoc($result);
    echo '<div id="container text-align:center;" style="width:100%;" data-aos="zoom-in-up">
    <div class="card text-center" style="width:100%; margin:0 auto;">
   
    <a href="#"><img src="'.$_POST["image_path"].'" class="img-responsive img-thumbnail catHPG wiggle mt-2" align="center"></a></div>
              <div class="container text-center">
                  <br>
                  <br>
                  <label id="greentext" style:"text-align:center;">Deskripsi Spesifikasi </label>
              </div>                
              <div class="container text-center" style="padding:3%;">

              <div class="col-lg-5"  style="padding-left:100px;"> 
                <label style="padding-bottom:2px;">Merek &nbsp : &nbsp</label><input type="text" value="'.$merek.'" placeholder="Merek" style="text-align:center;" readonly><br>
                <label style="padding-bottom:2px;">Tipe  &nbsp &nbsp &nbsp : &nbsp</label><input type="text" value="'. $tipe .'" name="tipe" style="text-align:center;" placeholder="Input" readonly><br>
                <label style="padding-bottom:2px;">Range Estimasi:</label><input type="text" value="'. $_POST['range_harga'] .'" name="tipe" style="text-align:center;" placeholder="Input" size="30" readonly><br><br>
             </div>
                <b>SPESIFIKASI GLOBAL : </b><br><br>
                - Fungsional berfungsi dengan Baik<br>
                - Fitur Tambahan Lengkap<br>
                - Kondisi Fisik Baik<br>
              </div>      
          <div class="container text-center">
            <!-- <a href="HalamanProduk.php"><button class="btn btn-success" style="margin-right:2%;">Ganti Barang</button></a> -->
            <a href="FormKelayakan.php" style="padding-left:3%;"><button class="btn btn-success" style="margin-right:2%;">Estimasi barang sesuai merek diatas</button></a>
          </div>

        </div>
      </div>
    </div>
  </div>
    </div>';
         ?>

        <!-- FOOTER -->
        <footer id="footer" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-8">
                         <div class="footer-info">
                              <div class="section-title">
                              <h5 class="font-weight-bold text-uppercase mb-2 wow fadeInUp" data-wow-delay="0.2s" >SIMPENAN GADAI</h5>
                              <p  class="wow fadeInUp" data-wow-delay="0.2s"><em>"Sistem Pengambil Keputusan Gadai"</em></p>
                              <p  class="wow fadeInUp" data-wow-delay="0.2s">Tugas Mini Project - SPRINT Angakatan 1 2020</p>
                              </div>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-8">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2 class="wow fadeInUp" data-wow-delay="0.2s">Alamat</h2>
                              </div>
                              <ul class="list-unstyled wow fadeInUp" data-wow-delay="0.2s">
                                   <li>
                                   <p>
                                        <i class="fas fa-home mr-3"></i> Kantor Pusat: Jl. Kramat Raya 162 Jakarta Pusat 10430 </p>
                                   </li>
                                   <li>
                                   <p>
                                        <i class="fas fa-envelope mr-3"></i> pegadaian@pegadaian.co.id</p>
                                   </li>
                                   <li>
                                   <p>
                                        <i class="fas fa-phone mr-3"></i> 0213155550 | 02180635162 </p>
                                   </li>
                              </ul>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                         <br>
                         <h5 class="font-weight-bold text-uppercase mb-2 wow fadeInUp" style="padding-left:35%;" data-wow-delay="0.8s">Follow Us</h5><br>
                              <a type="button" class="btn-floating btn-fb fa-2x wow fadeInUp" style="padding-left:35%;" data-wow-delay="0.8s">
                              <i class="fab fa-facebook-f ico"></i>
                              </a> 
                              <a type="button" class="btn-floating btn-tw fa-2x wow fadeInUp" data-wow-delay="0.8s">
                                   <i class="fab fa-twitter ico"></i>
                              </a>
                              <a type="button" class="btn-floating btn-instagram fa-2x wow fadeInUp" data-wow-delay="0.8s">
                                   <i class="fab fa-instagram ico"></i>
                              </a>
                         <div class="wow fadeInUp copyright-text" data-wow-delay="0.8s"> 
                              <p style="padding-left:20%;"><br>Copyright 2020 &copy; Team4SPRINT
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>
    
    <script>
        $('.select2').select2();
     </script>
     <script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/jquery.stellar.min.js"></script>
     <script src="js/wow.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/jquery.magnific-popup.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>
     <script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
            <script>
                    $(function() {
                      $(document).scroll(function(){
                        var $nav = $("#mainNavbar");
                        $nav.toggleClass("scrolled", $(this).scrollTop() > $nav.height());
                        if($(this).scrollTop() > 0) {
                            $("nav a").css('color', 'white');
                        }
                        else{
                          $("nav a").css('color', 'green');
                        }
                      });
                      position = scroll;
                    });
                  </script>
              <script>
                  AOS.init();
                </script>
        </body>
</html>